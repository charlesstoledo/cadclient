-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 16-Mar-2018 às 14:23
-- Versão do servidor: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_contabilidade`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_declaracoes_contabeis_sped_ecd`
--

CREATE TABLE `tbl_declaracoes_contabeis_sped_ecd` (
  `id_declaracao_sped_ecd` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `ano_declaracao_sped_ecd` varchar(255) NOT NULL DEFAULT '',
  `situacao_declaracao_sped_ecd` varchar(255) DEFAULT '1',
  `situacao_oculta_declaracao_sped_ecd` varchar(10) DEFAULT '1',
  `data_envio_declaracao_sped_ecd` varchar(50) NOT NULL DEFAULT '',
  `numero_recibo_declaracao_sped_ecd` varchar(255) NOT NULL DEFAULT '',
  `transmitida_por_declaracao_sped_ecd` varchar(255) NOT NULL DEFAULT '',
  `obs_declaracao_sped_ecd` text NOT NULL,
  `cadastrado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `atualizado_declaracao_sped_ecd` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1',
  `excluido` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tbl_declaracoes_contabeis_sped_ecd`
--

INSERT INTO `tbl_declaracoes_contabeis_sped_ecd` (`id_declaracao_sped_ecd`, `id_empresa`, `ano_declaracao_sped_ecd`, `situacao_declaracao_sped_ecd`, `situacao_oculta_declaracao_sped_ecd`, `data_envio_declaracao_sped_ecd`, `numero_recibo_declaracao_sped_ecd`, `transmitida_por_declaracao_sped_ecd`, `obs_declaracao_sped_ecd`, `cadastrado`, `atualizado_declaracao_sped_ecd`, `status`, `excluido`) VALUES
(104, 156, '2017', '4', '4', '2018-01-29', '0', 'Everton', '', '2018-01-29 18:01:52', '2018-02-28 11:11:11', 1, 0),
(105, 158, '2017', '4', '4', '2018-01-29', '0', 'Everton', '', '2018-01-29 20:17:38', '2018-02-28 11:09:58', 1, 0),
(106, 157, '2017', '4', '4', '2018-01-29', '0', 'Everton', '', '2018-01-29 20:17:52', '2018-02-28 11:10:30', 1, 0),
(107, 159, '2017', '4', '4', '2018-01-29', '0', 'Everton', '', '2018-01-29 20:19:03', '2018-02-28 11:07:48', 1, 0),
(110, 155, '2017', '1', '1', ' ', ' ', ' ', '', '2018-01-29 22:07:12', '2018-02-28 11:11:53', 1, 0),
(111, 217, '2017', '4', '4', '', '', 'Everton', '', '2018-01-31 23:59:58', '2018-02-15 14:18:40', 1, 0),
(112, 65, '2017', '1', '1', '2017-12-28', '0', 'Clícia', '', '2018-02-01 15:04:13', '2018-02-24 09:40:07', 1, 0),
(113, 61, '2017', '4', '4', '', '', 'Clícia', '', '2018-02-01 15:15:31', '2018-02-24 10:18:07', 1, 0),
(114, 66, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:15:39', '2018-02-28 10:39:45', 1, 0),
(115, 67, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:15:48', '2018-02-28 10:39:56', 1, 0),
(116, 99, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:16:00', '2018-02-28 09:59:34', 1, 0),
(117, 68, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:17:55', '2018-02-24 10:07:03', 1, 0),
(118, 69, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:18:03', '2018-02-24 10:19:05', 1, 0),
(119, 98, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:18:10', '2018-02-24 09:37:29', 1, 0),
(120, 70, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:18:20', '2018-02-24 10:19:37', 1, 0),
(121, 71, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:18:28', '2018-02-24 09:38:46', 1, 0),
(122, 77, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:18:36', '2018-02-24 10:12:01', 1, 0),
(123, 74, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:18:44', '2018-02-24 10:08:50', 1, 0),
(124, 76, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:18:52', '2018-02-24 10:20:05', 1, 0),
(125, 78, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:18:59', '2018-02-24 10:20:36', 1, 0),
(126, 79, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:19:07', '2018-02-24 09:41:11', 1, 0),
(127, 80, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:19:15', '2018-02-24 10:21:07', 1, 0),
(128, 81, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:19:24', '2018-02-24 10:21:34', 1, 0),
(129, 83, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:19:35', '2018-02-24 09:41:50', 1, 0),
(130, 82, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:19:41', '2018-02-24 09:42:53', 1, 0),
(131, 64, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:19:57', '2018-02-24 11:42:32', 1, 0),
(132, 63, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:20:05', '2018-02-24 11:42:21', 1, 0),
(133, 62, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:20:14', '2018-02-24 10:18:37', 1, 0),
(134, 85, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:20:21', '2018-02-24 10:22:20', 1, 0),
(135, 87, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:20:29', '2018-02-26 08:21:35', 1, 0),
(136, 88, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:20:36', '2018-02-24 09:44:13', 1, 0),
(137, 89, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:20:43', '2018-02-24 10:24:07', 1, 0),
(138, 93, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:20:51', '2018-02-24 11:42:08', 1, 0),
(139, 92, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:21:00', '2018-02-24 10:16:14', 1, 0),
(140, 11, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:33:01', '2018-02-27 09:22:06', 1, 0),
(141, 12, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:33:23', '2018-02-28 15:16:44', 1, 0),
(142, 169, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:33:33', '2018-02-26 14:23:00', 1, 0),
(143, 13, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:33:40', '2018-02-28 15:23:11', 1, 0),
(144, 14, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:33:57', '2018-02-27 11:04:12', 1, 0),
(145, 15, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:34:07', '2018-02-28 15:22:56', 1, 0),
(146, 16, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:34:17', '2018-02-28 15:22:39', 1, 0),
(147, 17, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:34:33', '2018-02-27 11:17:07', 1, 0),
(148, 18, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:34:45', '2018-02-27 11:16:23', 1, 0),
(149, 200, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:34:56', '2018-02-24 09:24:54', 1, 0),
(150, 19, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:34:57', '2018-02-28 15:22:11', 1, 0),
(151, 20, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:35:07', '2018-02-28 15:21:45', 1, 0),
(152, 199, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:35:15', '2018-02-24 10:36:14', 1, 0),
(153, 21, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:35:17', '2018-02-28 15:21:14', 1, 0),
(154, 198, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:35:29', '2018-02-24 08:27:33', 1, 0),
(155, 197, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:35:39', '2018-02-24 11:13:48', 1, 0),
(156, 196, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:35:49', '2018-02-24 10:24:25', 1, 0),
(157, 23, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:36:01', '2018-02-27 09:20:58', 1, 0),
(158, 195, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:36:05', '2018-02-24 10:15:14', 1, 0),
(159, 24, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:36:15', '2018-02-27 09:21:36', 1, 0),
(160, 194, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:36:20', '2018-02-24 10:04:44', 1, 0),
(161, 25, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:36:24', '2018-02-28 15:20:52', 1, 0),
(162, 193, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:36:28', '2018-02-24 09:51:57', 1, 0),
(163, 26, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:36:34', '2018-02-28 15:20:16', 1, 0),
(164, 192, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:36:36', '2018-02-24 09:12:10', 1, 0),
(165, 27, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:36:46', '2018-02-28 15:19:29', 1, 0),
(166, 191, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:36:53', '2018-02-24 09:42:23', 1, 0),
(167, 28, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:36:57', '2018-02-28 15:19:13', 1, 0),
(168, 190, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:37:06', '2018-02-24 09:33:50', 1, 0),
(169, 29, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:37:09', '2018-02-28 15:18:48', 1, 0),
(170, 30, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:37:22', '2018-02-28 15:16:21', 1, 0),
(171, 31, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:37:34', '2018-02-28 15:15:25', 1, 0),
(172, 32, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:37:47', '2018-02-28 15:14:46', 1, 0),
(173, 189, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:37:54', '2018-02-24 08:10:11', 1, 0),
(174, 33, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:37:55', '2018-02-28 15:12:42', 1, 0),
(175, 34, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:38:05', '2018-02-27 09:20:27', 1, 0),
(176, 188, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:38:11', '2018-02-24 10:49:04', 1, 0),
(177, 35, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:38:13', '2018-02-28 15:12:08', 1, 0),
(178, 187, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:38:22', '2018-02-24 11:28:28', 1, 0),
(179, 36, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:38:23', '2018-02-28 15:10:52', 1, 0),
(180, 37, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:38:31', '2018-02-28 15:07:36', 1, 0),
(181, 186, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:38:32', '2018-02-24 07:52:16', 1, 0),
(182, 38, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:38:42', '2018-02-27 08:57:00', 1, 0),
(183, 185, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:38:43', '2018-02-24 11:03:55', 1, 0),
(184, 39, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:38:52', '2018-02-28 15:07:07', 1, 0),
(185, 184, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:39:09', '2018-02-23 16:18:33', 1, 0),
(186, 183, '2017', '4', '4', '', '', 'Adrianne', '', '2018-02-01 15:39:21', '2018-03-13 16:44:37', 1, 0),
(187, 40, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:39:32', '2018-02-28 15:06:15', 1, 0),
(188, 171, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:39:49', '2018-02-26 13:33:16', 1, 0),
(189, 182, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:40:08', '2018-02-27 08:38:26', 1, 0),
(190, 41, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:40:19', '2018-02-28 15:05:50', 1, 0),
(191, 42, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:40:29', '2018-02-27 08:52:56', 1, 0),
(192, 43, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:40:37', '2018-02-27 08:52:17', 1, 0),
(193, 172, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:40:48', '2018-02-26 16:08:06', 1, 0),
(194, 44, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:40:51', '2018-02-27 08:51:11', 1, 0),
(195, 45, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:41:02', '2018-02-27 08:50:37', 1, 0),
(196, 170, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:41:02', '2018-02-26 10:59:05', 1, 0),
(197, 46, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:41:09', '2018-02-28 15:05:22', 1, 0),
(198, 47, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:41:17', '2018-02-28 15:04:31', 1, 0),
(199, 166, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:41:24', '2018-02-27 17:05:00', 1, 0),
(200, 48, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:41:27', '2018-02-28 15:03:52', 1, 0),
(201, 165, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:41:33', '2018-02-20 17:31:00', 1, 0),
(202, 49, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:41:34', '2018-02-28 15:03:30', 1, 0),
(203, 50, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:41:42', '2018-02-27 08:45:31', 1, 0),
(204, 164, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:41:48', '2018-02-23 09:00:53', 1, 0),
(205, 51, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:41:51', '2018-02-28 15:03:10', 1, 0),
(206, 52, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:41:58', '2018-02-28 15:02:41', 1, 0),
(207, 163, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:42:00', '2018-02-27 16:59:33', 1, 0),
(208, 53, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:42:07', '2018-02-27 08:43:15', 1, 0),
(209, 162, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:42:11', '2018-02-27 17:02:32', 1, 0),
(210, 54, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:42:16', '2018-02-27 08:43:44', 1, 0),
(211, 174, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:42:23', '2018-02-26 13:51:22', 1, 0),
(212, 55, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:42:23', '2018-02-27 08:42:27', 1, 0),
(213, 56, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:42:31', '2018-02-27 08:41:53', 1, 0),
(214, 173, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:42:33', '2018-02-01 09:42:33', 1, 0),
(215, 57, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:42:41', '2018-02-27 08:41:24', 1, 0),
(216, 161, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:42:46', '2018-02-27 09:08:51', 1, 0),
(217, 58, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:42:48', '2018-02-27 08:41:02', 1, 0),
(218, 59, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:42:56', '2018-02-27 08:39:36', 1, 0),
(219, 60, '', '4', '4', '', '', 'Adrianne', '', '2018-02-01 15:43:03', '2018-03-14 16:38:28', 1, 0),
(220, 160, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:43:04', '2018-02-26 15:36:42', 1, 0),
(221, 175, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:43:19', '2018-02-26 14:38:48', 1, 0),
(222, 176, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:45:03', '2018-02-26 15:19:35', 1, 0),
(223, 177, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:45:12', '2018-02-27 08:50:14', 1, 0),
(224, 178, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:45:22', '2018-02-01 09:45:22', 1, 0),
(225, 179, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:45:37', '2018-02-27 08:53:03', 1, 0),
(226, 180, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:52:28', '2018-02-01 09:52:28', 1, 0),
(227, 181, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 15:52:38', '2018-02-26 14:04:54', 1, 0),
(228, 145, '2017', '4', '4', '', '', 'Everton', '', '2018-02-01 16:08:48', '2018-02-15 14:22:26', 1, 0),
(229, 135, '2017', '4', '4', '', '', 'Everton', '', '2018-02-01 16:09:01', '2018-02-15 13:53:11', 1, 0),
(230, 116, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:09:16', '2018-02-26 17:35:10', 1, 0),
(231, 142, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:09:29', '2018-02-28 11:16:38', 1, 0),
(232, 126, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:09:39', '2018-02-28 11:23:01', 1, 0),
(233, 108, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:09:58', '2018-02-28 11:26:36', 1, 0),
(234, 127, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:12:35', '2018-02-28 11:21:22', 1, 0),
(235, 129, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:12:50', '2018-02-28 11:19:16', 1, 0),
(236, 150, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:13:03', '2018-02-28 11:15:10', 1, 0),
(237, 124, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:13:17', '2018-02-28 11:23:29', 1, 0),
(238, 128, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:13:46', '2018-02-28 11:20:56', 1, 0),
(239, 146, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:14:21', '2018-02-01 10:14:21', 1, 0),
(240, 123, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:14:34', '2018-02-24 10:25:20', 1, 0),
(242, 122, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:14:55', '2018-02-28 11:23:52', 1, 0),
(243, 148, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:15:08', '2018-02-28 11:15:52', 1, 0),
(244, 113, '2017', '4', '4', '', '', 'Adrianne', '', '2018-02-01 16:15:24', '2018-03-13 16:41:01', 1, 0),
(245, 218, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:17:49', '2018-02-01 10:17:49', 1, 0),
(246, 149, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:18:23', '2018-02-28 11:15:31', 1, 0),
(247, 109, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:26:53', '2018-02-28 11:26:17', 1, 0),
(248, 147, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:27:17', '2018-02-28 10:39:26', 1, 0),
(249, 119, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:27:30', '2018-02-28 10:38:29', 1, 0),
(250, 120, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:27:41', '2018-02-28 11:25:12', 1, 0),
(251, 134, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:27:55', '2018-02-28 11:17:46', 1, 0),
(252, 121, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:28:45', '2018-02-28 11:24:13', 1, 0),
(253, 130, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:28:57', '2018-02-28 11:18:48', 1, 0),
(254, 114, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:29:37', '2018-02-26 09:26:16', 1, 0),
(255, 110, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:32:49', '2018-02-28 11:25:33', 1, 0),
(256, 103, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:33:04', '2018-02-28 11:28:59', 1, 0),
(257, 104, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:33:17', '2018-02-28 11:28:36', 1, 0),
(258, 105, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:33:38', '2018-02-26 16:25:04', 1, 0),
(259, 151, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:33:49', '2018-02-28 11:14:51', 1, 0),
(260, 152, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:34:20', '2018-02-28 11:34:31', 1, 0),
(261, 154, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:34:31', '2018-02-28 11:34:51', 1, 0),
(262, 131, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:34:47', '2018-02-28 11:35:14', 1, 0),
(263, 106, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:35:03', '2018-02-28 11:35:34', 1, 0),
(264, 118, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:35:13', '2018-02-26 11:59:00', 1, 0),
(265, 107, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:35:28', '2018-02-28 11:35:53', 1, 0),
(266, 111, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:35:41', '2018-02-24 09:51:24', 1, 0),
(267, 112, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:35:49', '2018-02-24 11:42:45', 1, 0),
(268, 133, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:36:00', '2018-02-28 11:32:03', 1, 0),
(269, 140, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:36:11', '2018-02-01 10:36:11', 1, 0),
(270, 139, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:36:25', '2018-02-06 07:56:07', 1, 0),
(271, 115, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-01 16:37:03', '2018-02-26 17:21:40', 1, 0),
(272, 117, '2017', '4', '4', '', '', 'Adrianne', '', '2018-02-01 16:37:18', '2018-03-13 16:43:33', 1, 0),
(273, 153, '2017', '4', '4', '', '', 'Everton', '', '2018-02-01 16:37:29', '2018-02-28 11:14:01', 1, 0),
(274, 220, '2017', '4', '4', '', '', 'Everton', '', '2018-02-05 15:28:11', '2018-02-15 09:55:02', 1, 0),
(278, 137, '2017', '4', '4', '', '', 'Everton', '', '2018-02-15 20:19:32', '2018-02-15 14:21:59', 1, 0),
(279, 75, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-24 16:10:17', '2018-02-24 10:10:48', 1, 0),
(280, 72, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-24 16:10:26', '2018-02-24 10:11:13', 1, 0),
(281, 84, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-24 16:12:57', '2018-02-24 10:13:18', 1, 0),
(282, 91, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-24 16:17:07', '2018-02-24 10:17:28', 1, 0),
(283, 90, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-24 16:24:27', '2018-02-24 10:24:48', 1, 0),
(284, 86, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-26 16:12:53', '2018-02-26 10:13:17', 1, 0),
(285, 141, '2017', '1', '1', ' ', ' ', ' ', '', '2018-02-28 17:16:57', '2018-02-28 11:31:26', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_declaracoes_contabeis_sped_ecd`
--
ALTER TABLE `tbl_declaracoes_contabeis_sped_ecd`
  ADD PRIMARY KEY (`id_declaracao_sped_ecd`),
  ADD KEY `fk_tbl_declaracoes_contabeis_sped_ecd_id_empresa` (`id_empresa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_declaracoes_contabeis_sped_ecd`
--
ALTER TABLE `tbl_declaracoes_contabeis_sped_ecd`
  MODIFY `id_declaracao_sped_ecd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=286;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tbl_declaracoes_contabeis_sped_ecd`
--
ALTER TABLE `tbl_declaracoes_contabeis_sped_ecd`
  ADD CONSTRAINT `fk_tbl_declaracoes_contabeis_sped_ecd_id_empresa` FOREIGN KEY (`id_empresa`) REFERENCES `tbl_empresas` (`id_empresa`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
