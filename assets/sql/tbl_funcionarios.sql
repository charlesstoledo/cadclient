--CRIAÇÃO DA TABELA DE FUNCIONÁRIOS
CREATE TABLE `tbl_funcionarios` (
  `id_funcionario` int(10) PRIMARY KEY AUTO_INCREMENTE NOT NULL , 
  `id_usuario` varchar(255),
  `nome_funcionario` varchar(255),
  `departamento_funcionario` varchar(255),
  `equipe_funcionario` varchar(255),
  `funcao_funcionario` varchar(255),
  `data_admissao_funcionario` varchar(255),
  `data_demissao_funcionario` varchar(255),
  `observacoes_funcionario` varchar(255),
  `cadastrado` timestamp DEFAULT CURRENT_TIMESTAMP,
  `atualizado` datetime,
  `status` tinyint(1),
  `excluido` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `tbl_funcionarios`
  ADD KEY `id_usuario` (`id_usuario`);

ALTER TABLE `tbl_funcionarios`
  ADD CONSTRAINT `tbl_funcionarios_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuarios` (`id_usuario`);
COMMIT;
