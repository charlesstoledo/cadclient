/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Charles
 * Created: 03/07/2017
 */

CREATE TABLE IF NOT EXISTS tbl_admin_log (
  id int(10) NOT NULL AUTO_INCREMENT,
  nome_admin varchar(255) DEFAULT NULL,
  acesso_admin timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  ip_admin varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;