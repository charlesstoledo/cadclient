/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Charles
 * Created: 03/07/2017
 */

CREATE TABLE IF NOT EXISTS tbl_usuarios (
  id_usuario int(10) unsigned NOT NULL AUTO_INCREMENT,
  usuario varchar(45) DEFAULT NULL,
  email varchar(45) DEFAULT NULL,
  senha varchar(255) DEFAULT NULL,
  ativo tinyint(1) DEFAULT '1',
  cadastrado_em timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id_usuario)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;