/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Charles
 * Created: 03/07/2017
 */

-- CREATE TABLE IF NOT EXISTS ci_sessions(
--     session_id varchar(40) DEFAULT '0' NOT NULL,
--     ip_address varchar(16) DEFAULT '0' NOT NULL,
--     user_agent varchar(120) NOT NULL,
--     last_activity int(10) unsigned DEFAULT 0 NOT NULL,
--     user_data text NOT NULL,
--     PRIMARY KEY (session_id),
--     KEY last_activity_idx (last_activity)
--     );

CREATE TABLE IF NOT EXISTS ci_sessions (
  id varchar(128) NOT NULL,
  ip_address varchar(45) NOT NULL,
  timestamp int(10) unsigned NOT NULL DEFAULT '0',
  data blob NOT NULL,
  PRIMARY KEY (id),
  KEY ci_sessions_timestamp (timestamp)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- echo date("d/m/Y H:i:s", $n);