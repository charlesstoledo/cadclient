-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 15-Dez-2017 às 21:32
-- Versão do servidor: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_funorte`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `acesso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `acesso`, `data`) VALUES
('12t1skeemb7hu5pvcmranqdmene3eau4', '::1', 1513368847, '2017-12-15 20:09:04', 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333336383534343b),
('4u485reuqq2sj2fc79s06q9f95eimcft', '::1', 1513367227, '2017-12-15 19:44:56', 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333336373039363b),
('9o1ljjthv2f4jaum7k6ti8m6n9iqgpj9', '::1', 1513369107, '2017-12-15 20:15:27', 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333336383932373b7573756172696f7c733a343a22526f6f74223b6c6f6761646f7c623a313b),
('d5g25je7lgta6ot08h6numc8hlqt8ltu', '::1', 1512413864, '2017-12-04 18:57:34', 0x5f5f63695f6c6173745f726567656e65726174657c693a313531323431333835343b),
('jrjrqhk14p43kqnvqhd34ftsq44rs023', '::1', 1513368185, '2017-12-15 20:02:15', 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333336383133343b),
('ksrs63devtqsr29ic7os502dbou88iio', '::1', 1513369910, '2017-12-15 20:31:50', 0x5f5f63695f6c6173745f726567656e65726174657c693a313531333336393931303b),
('rhvkcnhbeqdkc1l0014jkvekqhphb3t3', '::1', 1512412179, '2017-12-04 18:28:29', 0x5f5f63695f6c6173745f726567656e65726174657c693a313531323431323130393b);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_admin_log`
--

CREATE TABLE IF NOT EXISTS `tbl_admin_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nome_admin` varchar(255) DEFAULT NULL,
  `acesso_admin` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_admin` varchar(255) DEFAULT NULL,
  `status_admin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `tbl_admin_log`
--

INSERT INTO `tbl_admin_log` (`id`, `nome_admin`, `acesso_admin`, `ip_admin`, `status_admin`) VALUES
(1, 'root', '2017-11-23 20:04:14', '192.168.25.44', 1),
(2, 'charles', '2017-11-23 20:16:52', '192.168.25.42', 0),
(9, 'Root', '2017-12-15 20:31:23', '::1', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_banners`
--

CREATE TABLE IF NOT EXISTS `tbl_banners` (
  `id_banner` int(10) NOT NULL AUTO_INCREMENT,
  `imagem_banner` text,
  `titulo_banner` varchar(255) DEFAULT NULL,
  `texto_banner` varchar(255) NOT NULL,
  `link_banner` varchar(255) DEFAULT NULL,
  `cadastrado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `atualizado` datetime DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `excluido` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_banner`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `tbl_banners`
--

INSERT INTO `tbl_banners` (`id_banner`, `imagem_banner`, `titulo_banner`, `texto_banner`, `link_banner`, `cadastrado`, `atualizado`, `status`, `excluido`) VALUES
(1, 'e08fe7a0e9958acf28b5b684e0e2983a5a1d64118df13.jpg', 'Banner-1', 'Texto', 'http://www.google.com.br', '2017-08-28 20:49:44', '2017-11-28 10:36:32', '1', 0),
(2, '0fe4f8b5958db18606f528ab05035c915a1d6421a4cfa.jpg', 'Banner-2', 'Texto', 'http://www.google.com.br', '2017-08-28 20:49:23', '2017-11-28 10:36:28', '1', 0),
(3, '547530a10d97bd8f3b2c834b1d63f2be5a1d642a353bc.jpg', 'Banner-3', 'Texto', 'http://www.google.com.br', '2017-08-28 20:49:00', '2017-11-28 10:36:08', '1', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_biblioteca`
--

CREATE TABLE IF NOT EXISTS `tbl_biblioteca` (
  `id_documento` int(10) NOT NULL AUTO_INCREMENT,
  `titulo_documento` varchar(255) DEFAULT NULL,
  `link_documento` varchar(255) DEFAULT NULL,
  `descricao_documento` text,
  `autor_documento` varchar(255) DEFAULT NULL,
  `cadastrado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `atualizado` datetime DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `excluido` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_documento`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `tbl_biblioteca`
--

INSERT INTO `tbl_biblioteca` (`id_documento`, `titulo_documento`, `link_documento`, `descricao_documento`, `autor_documento`, `cadastrado`, `atualizado`, `status`, `excluido`) VALUES
(8, 'Certificação Linux', '32a7aa5e33a8af84b735ba96e8fed65b5a18312058099.pdf', 'Exemplo de como se elaborar um documento para certificação Linux, distribuição <b>CentOS 7.</b>', 'Charles de Azevedo Júnior', '2017-11-24 14:48:00', '2017-11-24 11:48:00', '1', 0),
(9, 'Teste', '4575a97fdbf74d2186e953a7fef47c4f5a1f131d744a8.pdf', 'Teste', 'Teste', '2017-11-29 20:05:49', '2017-11-29 17:05:49', '1', 0),
(10, 'asdasda', '02bca27be9fd40f6ebac9712d6c6560b5a1f190275046.pdf', 'sdasda', 'sdasd', '2017-11-29 20:30:58', '2017-11-29 17:30:58', '1', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_contatos`
--

CREATE TABLE IF NOT EXISTS `tbl_contatos` (
  `id_contato` int(10) NOT NULL AUTO_INCREMENT,
  `mapa_contato` text,
  `endereco_contato` text,
  `fixo_contato` varchar(255) DEFAULT NULL,
  `celular_contato` varchar(255) DEFAULT NULL,
  `email_contato` varchar(255) DEFAULT NULL,
  `whatsapp_contato` varchar(255) DEFAULT NULL,
  `facebook_contato` text,
  `twitter_contato` text,
  `instagram_contato` text,
  `cadastrado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `atualizado` datetime DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `excluido` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_contato`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `tbl_contatos`
--

INSERT INTO `tbl_contatos` (`id_contato`, `mapa_contato`, `endereco_contato`, `fixo_contato`, `celular_contato`, `email_contato`, `whatsapp_contato`, `facebook_contato`, `twitter_contato`, `instagram_contato`, `cadastrado`, `atualizado`, `status`, `excluido`) VALUES
(2, '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d286851.9103813513!2d-122.1651158!3d37.4064828!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808fa6b1117280ff%3A0xebbf998e5df289ab!2sMenlo+Park%2C+CA!5e0!3m2!1sen!2s!4v1471984430727" height="300"></iframe>', '<p><font face="Arial" style="">Rua Coronel Eufrásio Câmara, 546</font></p><p><font face="Arial" style="">Monte Santo - Campina Grande,PB </font></p><p><font face="Arial" style="">CEP: 58.400-696</font></p>', '+55 083 3321-7596', '+55 083 9-9956-5526', 'funorte@funortecampinagrande.com.br', '+55 9-9966-3322', 'http://facebook.com.br', 'http://twitter.com.br', 'http://instagram.com.br', '2017-11-24 12:27:06', '2017-11-28 15:41:15', '1', 0),
(3, '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3958.168841628926!2d-35.90143468466827!3d-7.221573872917146!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7ac1e30f60ad903%3A0xb9fb0cf9860661cc!2sFunorte!5e0!3m2!1spt-BR!2sbr!4v1511527786707"  height="300" ></iframe>', '<div jstcache="36" class="place-name" jsan="7.place-name" style="user-select: text; overflow: hidden; text-overflow: ellipsis;"><font color="#5b5b5b" face="Roboto, Arial"><span style="font-size: 12px; white-space: nowrap;">Rua José de Alencar, 775</span></font></div><div jstcache="36" class="place-name" jsan="7.place-name" style="user-select: text; overflow: hidden; text-overflow: ellipsis;"><font color="#5b5b5b" face="Roboto, Arial"><span style="font-size: 12px; white-space: nowrap;">Prata, Campina Grande - PB</span></font></div><div jstcache="36" class="place-name" jsan="7.place-name" style="user-select: text; overflow: hidden; text-overflow: ellipsis;"><font color="#5b5b5b" face="Roboto, Arial"><span style="font-size: 12px; white-space: nowrap;">CEP: 58400-500<br></span></font></div><div jstcache="36" class="place-name" jsan="7.place-name" style="user-select: text; overflow: hidden; text-overflow: ellipsis;"><br></div>', '+55 083 3322-7835', '+55 083 3322-7835', 'funorte@funortecampinagrande.com.br', '+55 083 3322-7835', 'http://facebook.com.br', 'http://twitter.com.br', 'http://instagram.com.br', '2017-11-24 12:52:06', '2017-11-28 15:11:41', NULL, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_cursos`
--

CREATE TABLE IF NOT EXISTS `tbl_cursos` (
  `id_curso` int(10) NOT NULL AUTO_INCREMENT,
  `imagem_curso` text,
  `titulo_curso` varchar(255) DEFAULT NULL,
  `link_curso` varchar(255) DEFAULT NULL,
  `descricao_curso` text,
  `objetivo_curso` text,
  `conteudo_curso` text,
  `horario_curso` varchar(255) DEFAULT NULL,
  `data_inicio_curso` varchar(255) DEFAULT NULL,
  `professores_curso` varchar(255) DEFAULT NULL,
  `cordenador_curso` varchar(255) DEFAULT NULL,
  `certificado_curso` varchar(255) DEFAULT NULL,
  `tipo_curso` varchar(255) DEFAULT NULL,
  `popular_curso` enum('0','1') DEFAULT '1',
  `publico_alvo_curso` text,
  `formas_pagamento_curso` varchar(255) DEFAULT NULL,
  `duracao_curso` text,
  `destaque_curso` enum('0','1') NOT NULL DEFAULT '0',
  `cadastrado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `atualizado` datetime DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `excluido` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_curso`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

--
-- Extraindo dados da tabela `tbl_cursos`
--

INSERT INTO `tbl_cursos` (`id_curso`, `imagem_curso`, `titulo_curso`, `link_curso`, `descricao_curso`, `objetivo_curso`, `conteudo_curso`, `horario_curso`, `data_inicio_curso`, `professores_curso`, `cordenador_curso`, `certificado_curso`, `tipo_curso`, `popular_curso`, `publico_alvo_curso`, `formas_pagamento_curso`, `duracao_curso`, `destaque_curso`, `cadastrado`, `atualizado`, `status`, `excluido`) VALUES
(8, 'a0a073d60721aa14420683594be68f155a1d68ca13fc4.png', 'qqqqqqqqqqqq', 'qqqqqqqqqqq', 'qqqqqqqqqqqqqqqqqqqqqqqq', 'wwwwwwwwwwwwwwwwwwwwwwwww', 'wwwwwwwwwwwwwwwwwwwwwww', 'qqweqweweeweqeweq', 'qeqwewqewqwewqe', '1;2;3;4;', '1', 'qewqeqwwwqqeqwwqee', '1', '1', 'qeqwewqewqewqeqwwq', 'qwwqwwwqeweqwq', 'qwwqeqewqeqw', '1', '2017-11-27 13:55:53', '2017-11-28 10:54:37', '1', 0),
(58, '8ab12d589ceb245ad081cbe9a431e3945a1d68b6b0c1e.png', 'PHP  7', 'www.google.com.br', 'Curso completo de PHP', 'Tornar você em um programador php', 'Do básico ao avançado!', '8h - 12h', '15 de Janeiro de 2018', '1;2;', '1', 'Personal Home Page Seven', '2', '1', 'Programadores&nbsp;', 'Até em 12 x nos cartões', '80 horas aulas', '1', '2017-11-27 18:10:53', '2017-11-28 10:54:30', '1', 0),
(59, '8ab12d589ceb245ad081cbe9a431e3945a1d68fdb7d4c.png', 'asdasd', 'asdasdsa', '<ol><li>dasdasd</li></ol>', 'adad', 'adad', 'asda', 'dasd', '1;2;', '6', 'asdasd', '1', '0', 'asdasd', 'addas', 'asdasd', '1', '2017-11-28 13:47:41', '2017-11-28 15:59:40', '1', 0),
(60, '8cef75cab0dc30960999c0374faa01f65a1d6939193ca.png', 'wqedwqeqwe', 'qweweqw', '<ol><li>eqweqwe</li></ol>', 'qweqwee', 'qeqwe', 'qweqee', 'qeqwe', '2;3;', '1', 'qweqwe', '3', '0', 'qweqwe', 'qweqwe', 'qweqwe', '0', '2017-11-28 13:48:41', '2017-11-28 15:59:10', '1', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_depoimentos`
--

CREATE TABLE IF NOT EXISTS `tbl_depoimentos` (
  `id_depoimento` int(10) NOT NULL AUTO_INCREMENT,
  `imagem_depoimento` text,
  `titulo_depoimento` varchar(255) DEFAULT NULL,
  `pessoa_depoimento` varchar(255) DEFAULT NULL,
  `descricao_depoimento` text,
  `cadastrado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `atualizado` datetime DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `excluido` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_depoimento`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `tbl_depoimentos`
--

INSERT INTO `tbl_depoimentos` (`id_depoimento`, `imagem_depoimento`, `titulo_depoimento`, `pessoa_depoimento`, `descricao_depoimento`, `cadastrado`, `atualizado`, `status`, `excluido`) VALUES
(2, '5a6307fa272e6d738fbd5f0f74da3a885a1d98f2319f1.jpg', 'Excelentes Professores', 'Juliano da Silva', '<ol><li>Conhecedores dos conteúdos;</li><li>Simpáticos;</li><li>Atenciosos;&nbsp;</li></ol>', '2017-11-23 12:31:13', '2017-11-28 14:12:18', '1', 0),
(5, '0cc894ee6377a8ba378bf7be3b1fac6a5a1d98ea478c0.jpg', 'Ótimos Cursos', 'Tadeu Araújo', '<font face="Helvetica">"Adorei a <u>metodologia</u> de ensino e a abordagem dos assuntos mais complexos."</font>', '2017-11-23 12:37:53', '2017-11-28 14:24:17', '1', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_galeria`
--

CREATE TABLE IF NOT EXISTS `tbl_galeria` (
  `id_foto` int(10) NOT NULL AUTO_INCREMENT,
  `foto` text,
  `titulo_foto` varchar(255) DEFAULT NULL,
  `cadastrado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `atualizado` datetime DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `excluido` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_foto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Extraindo dados da tabela `tbl_galeria`
--

INSERT INTO `tbl_galeria` (`id_foto`, `foto`, `titulo_foto`, `cadastrado`, `atualizado`, `status`, `excluido`) VALUES
(4, '7e538b3634318febaf20b4f0c43430bc5a1da27c4e479.jpg', 'Lorem', '2017-11-23 17:20:14', '2017-11-28 14:53:00', '1', 0),
(5, '833112c3ed6ab28711337160e65e58665a1da273bb7ba.jpg', 'Ipsum', '2017-11-23 17:20:27', '2017-11-28 14:52:51', '1', 0),
(6, 'ede96c235f6b0cfabf275acfeb138d575a1da2684d632.jpg', 'Lorem Ipsum', '2017-11-23 17:20:39', '2017-11-28 14:55:00', NULL, 0),
(7, '82dfd840ceb21d46a07cada58a3fd9575a1da28aa98d8.jpg', 'asdasdasd', '2017-11-28 17:53:14', '2017-11-28 14:53:14', '1', 0),
(8, '5b3162fb0c5718bfde127a3650a826465a1da2913f0f5.jpg', 'adadada', '2017-11-28 17:53:21', '2017-11-28 14:53:21', '1', 0),
(9, 'f8a609ed5b4e9d14a18140e0c5796fda5a1da2960bbaf.jpg', 'adadadasd', '2017-11-28 17:53:26', '2017-11-28 14:53:26', '1', 0),
(10, '35398e799338b81168f1e51a086baecd5a1da29b45b82.jpg', 'adadasdd', '2017-11-28 17:53:31', '2017-11-28 14:53:31', '1', 0),
(11, '90684530830fb7d023b0b0b69c6fa22e5a1da2a0baf0f.jpg', 'adadasdasdadd', '2017-11-28 17:53:36', '2017-11-28 14:53:36', '1', 0),
(12, '7e6164388a9ec5edf28a932d9ccd67ad5a1da2a65396c.jpg', 'adasdasdada', '2017-11-28 17:53:42', '2017-11-28 14:53:42', '1', 0),
(13, '2601dd57bdae0819ac2afcd7c291c25e5a1da2ab81365.jpg', 'adadad', '2017-11-28 17:53:47', '2017-11-28 17:25:54', NULL, 0),
(14, 'c34b2b46f735b59b6f24a01f20f72c2b5a1da2b13ce25.jpg', 'adasdasdasd', '2017-11-28 17:53:53', '2017-11-28 14:53:53', '1', 0),
(15, '5c71077fa5a7c69566cee73a5292cf305a1da2b7cf711.jpg', 'Aqui vai o título', '2017-11-28 17:53:59', '2017-11-28 17:28:33', '1', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_institucional`
--

CREATE TABLE IF NOT EXISTS `tbl_institucional` (
  `id_institucional` int(10) NOT NULL AUTO_INCREMENT,
  `imagem_institucional` text,
  `titulo_institucional` varchar(255) DEFAULT NULL,
  `subtitulo_institucional` varchar(255) DEFAULT NULL,
  `descricao_institucional` text,
  `cadastrado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `atualizado` datetime DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `excluido` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_institucional`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `tbl_institucional`
--

INSERT INTO `tbl_institucional` (`id_institucional`, `imagem_institucional`, `titulo_institucional`, `subtitulo_institucional`, `descricao_institucional`, `cadastrado`, `atualizado`, `status`, `excluido`) VALUES
(3, 'f99687dd719c4e8bc6a39e946c3d9ef75a16d55000929.jpg', 'O que é Lorem Ipsum?', 'Lorem Ipsum', '<span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</span>', '2017-11-23 14:03:43', '2017-11-28 14:40:43', NULL, 0),
(4, 'a94e097e4382958549ac81464c6e3efd5a1d9f694edba.jpg', 'De onde ele <span>vem</span>?', 'Lorem Ipsum', '<p><span style="text-align: justify;">Ao contrário do que se acredita, <span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;">Lorem Ipsum não é simplesmente um texto randômico. Com mais de 2000 anos, suas raízes podem ser encontradas em uma obra de literatura latina clássica datada de 45 AC. Richard McClintock, um professor de latim do Hampden-Sydney College na Virginia, pesquisou uma das mais obscuras palavras em latim, consectetur, oriunda de uma passagem de Lorem Ipsum, e, procurando por entre citações da palavra na literatura clássica, descobriu a sua indubitável origem.</span></span></p><p><span style="text-align: justify;">Ao contrário do que se acredita,&nbsp;</span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem Ipsum&nbsp;não é simplesmente um texto randômico. Com mais de 2000 anos, suas raízes podem ser encontradas em uma obra de literatura latina clássica datada de 45 AC. Richard McClintock, um professor de latim do Hampden-Sydney College na Virginia, pesquisou uma das mais obscuras palavras em latim, consectetur, oriunda de uma passagem de&nbsp;Lorem Ipsum.</span><span style="text-align: justify;"><br></span><br></p>', '2017-11-23 14:06:18', '2017-11-28 15:44:44', '1', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_noticias`
--

CREATE TABLE IF NOT EXISTS `tbl_noticias` (
  `id_noticia` int(10) NOT NULL AUTO_INCREMENT,
  `imagem_pequena_noticia` text,
  `imagem_noticia` text,
  `titulo_noticia` varchar(255) DEFAULT NULL,
  `link_noticia` varchar(255) DEFAULT NULL,
  `descricao_noticia` text,
  `horario_noticia` varchar(255) DEFAULT NULL,
  `local_noticia` varchar(255) DEFAULT NULL,
  `mapa_noticia` text,
  `data_noticia` varchar(255) DEFAULT NULL,
  `cadastrado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `atualizado` datetime DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `excluido` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_noticia`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `tbl_noticias`
--

INSERT INTO `tbl_noticias` (`id_noticia`, `imagem_pequena_noticia`, `imagem_noticia`, `titulo_noticia`, `link_noticia`, `descricao_noticia`, `horario_noticia`, `local_noticia`, `mapa_noticia`, `data_noticia`, `cadastrado`, `atualizado`, `status`, `excluido`) VALUES
(4, '6e3a51fa5ae137f6d2bac913542840e45a1db346bd455.jpg', 'e56be7477d781b4ee465bf9844e2c1945a1db346bd8c2.jpg', 'O que é Lorem Ipsum?', 'http://www.google.com', '<strong style="margin: 0px; padding: 0px; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem Ipsum</strong><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">&nbsp;é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado.</span>', '15h30', 'Parque do povo', '                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d286851.9103813513!2d-122.1651158!3d37.4064828!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808fa6b1117280ff%3A0xebbf998e5df289ab!2sMenlo+Park%2C+CA!5e0!3m2!1sen!2s!4v1471984430727" height="225"></iframe>    ', '23/11/2017', '2017-11-23 18:40:02', '2017-11-28 16:17:59', '1', 0),
(5, '0cd9ae1c66de01dfecf3008d728bbb655a1db32bc74ef.jpg', '70a6afda95917c31b7f7394bab0236ea5a1db32bc7a45.jpg', 'Porque nós o usamos?', 'http://www.google.com.br', '<p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;"><b>É</b> um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar<u> Lorem Ipsum</u> é que ele tem uma distribuição normal de letras, ao contrário de "Conteúdo aqui, conteúdo aqui", fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por ''lorem ipsum'' mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).</span></p>', '16h00', 'Campina Grande', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d286851.9103813513!2d-122.1651158!3d37.4064828!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808fa6b1117280ff%3A0xebbf998e5df289ab!2sMenlo+Park%2C+CA!5e0!3m2!1sen!2s!4v1471984430727" height="300"></iframe>', '24/11/2017', '2017-11-23 19:00:41', '2017-11-28 16:09:18', '1', 0),
(6, '0cd9ae1c66de01dfecf3008d728bbb655a1db76a998eb.jpg', '70a6afda95917c31b7f7394bab0236ea5a1db76a99e38.jpg', 'Onde posso conseguí-lo?', '!', '<span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">Existem muitas variações disponíveis de passagens de Lorem Ipsum, mas a maioria sofreu algum tipo de alteração, seja por inserção de passagens com humor, ou palavras aleatórias que não parecem nem um pouco convincentes. Se você pretende usar uma passagem de Lorem Ipsum, precisa ter certeza de que não há algo embaraçoso escrito escondido no meio do texto. Todos os geradores de Lorem Ipsum na internet tendem a repetir pedaços predefinidos conforme necessário, fazendo deste o primeiro gerador de Lorem Ipsum autêntico da internet. Ele usa um dicionário com mais de 200 palavras em Latim combinado com um punhado de modelos de estrutura de frases para gerar um Lorem Ipsum com aparência razoável, livre de repetições, inserções de humor, palavras não características, etc.</span>', '16h 20min', 'Campina Grande', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63327.73108230926!2d-35.936583813506!3d-7.242751164355782!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7ac1e5f43201c85%3A0xc9656aec3aa6af51!2sCampina+Grande%2C+PB!5e0!3m2!1spt-BR!2sbr!4v1511896871737" width="200" height="225" frameborder="0" style="border:0" allowfullscreen></iframe>', '28 de Novembro de 2017', '2017-11-28 19:22:18', '2017-11-28 16:23:53', '1', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_parceiros`
--

CREATE TABLE IF NOT EXISTS `tbl_parceiros` (
  `id_parceiro` int(10) NOT NULL AUTO_INCREMENT,
  `imagem_parceiro` text,
  `titulo_parceiro` varchar(255) DEFAULT NULL,
  `link_parceiro` varchar(255) DEFAULT NULL,
  `descricao_parceiro` text,
  `cadastrado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `atualizado` datetime DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `excluido` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_parceiro`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Extraindo dados da tabela `tbl_parceiros`
--

INSERT INTO `tbl_parceiros` (`id_parceiro`, `imagem_parceiro`, `titulo_parceiro`, `link_parceiro`, `descricao_parceiro`, `cadastrado`, `atualizado`, `status`, `excluido`) VALUES
(3, 'd36779885309cbeb4f1697ae0e97bff35a1d9a589148b.jpg', 'asdas', 'htttp://www.google.com.br', '<b style="background-color: rgb(255, 255, 0);">sdasd</b>', '2017-11-23 13:00:09', '2017-11-28 14:18:16', NULL, 0),
(16, '4a1dac255115fcf1ab1e1f24421be0fd5a1d9a4d0e182.jpg', 'asdas', 'htttp://www.google.com.br', 'asdad', '2017-11-23 13:04:06', '2017-11-28 14:18:05', '1', 0),
(17, 'd5541e31ab0f1b70b28cb1ab0ecbc90d5a1d9a434acd1.jpg', 'asda', 'htttp://www.google.com.br', 'adas', '2017-11-23 13:04:18', '2017-11-28 14:17:55', '1', 0),
(18, '3a754a3528ab61147797df64cd1039575a1d9a31e9c00.jpg', 'asdas', 'htttp://www.google.com.br', 'asdasd', '2017-11-23 13:04:28', '2017-11-28 14:17:37', '1', 0),
(19, 'c7770b0983dde4c84f5ddc6f940481705a1d9a77bd784.jpg', 'asdasd', 'htttp://www.google.com.br', 'sdasdad', '2017-11-28 17:18:47', '2017-11-28 14:18:47', '1', 0),
(20, 'd61ed018a0ac3017877aafd08a3340d35a1d9a84581b9.jpg', 'adaasd', 'htttp://www.google.com.br', 'aasca', '2017-11-28 17:19:00', '2017-11-28 14:19:00', '1', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_processo_seletivo`
--

CREATE TABLE IF NOT EXISTS `tbl_processo_seletivo` (
  `id_processo_seletivo` int(10) NOT NULL AUTO_INCREMENT,
  `titulo_processo_seletivo` varchar(255) DEFAULT NULL,
  `descricao_processo_seletivo` text,
  `cadastrado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `atualizado` datetime DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `excluido` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_processo_seletivo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_professores`
--

CREATE TABLE IF NOT EXISTS `tbl_professores` (
  `id_professor` int(10) NOT NULL AUTO_INCREMENT,
  `imagem_professor` text,
  `nome_professor` varchar(255) DEFAULT NULL,
  `titulo_professor` varchar(255) DEFAULT NULL,
  `cordenador_professor` enum('0','1') DEFAULT '0',
  `cadastrado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `atualizado` datetime DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `excluido` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_professor`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `tbl_professores`
--

INSERT INTO `tbl_professores` (`id_professor`, `imagem_professor`, `nome_professor`, `titulo_professor`, `cordenador_professor`, `cadastrado`, `atualizado`, `status`, `excluido`) VALUES
(1, '1fb1f26afd2e56833ee46ec8dc5abe105a1d9609c89b6.jpg', 'Tiago Melo', '<ul><li>Expert em Web Services;</li><li>Html 5 paper;</li><li>Css 3;</li><li>Js;</li><li>PHP 7;</li></ul>', NULL, '2017-11-22 17:37:44', '2017-11-28 14:22:41', '1', 0),
(2, '232daad99cbcc48ba661b999e6f215295a1d95f11c276.jpg', 'Mayara Tarço', '<ul><li><span style="background-color: rgb(0, 255, 0); color: rgb(255, 255, 255);">Inglês Advanced Pro;</span></li><li><span style="color: rgb(0, 0, 255);"> Tecnical Linguistic;</span>&nbsp;</li><li><b> Bassoral Canadensse;</b></li></ul>', NULL, '2017-11-22 18:06:17', '2017-11-28 13:59:29', '1', 0),
(3, '2fa900054c9cc587a94eac03771365fd5a1d95e547e9d.jpg', 'Marcelo Barbosa', '<ul><li>Professor especialista em códigos php;</li></ul>', '1', '2017-11-23 11:25:12', '2017-11-28 13:59:17', '1', 0),
(4, '3f8e06497214562bf4cfc4ca54e118c25a1d95d994839.jpg', 'Teste', 'Bassoral', NULL, '2017-11-24 19:37:04', '2017-11-28 13:59:05', '1', 0),
(5, 'ffe2935af578ff5240d36e9cf75234695a1d95cd31f6e.jpg', 'Wellington Santos', 'Especialista em web designer&nbsp;&nbsp;', NULL, '2017-11-27 18:20:40', '2017-11-28 13:58:53', '1', 0),
(6, '1fb1f26afd2e56833ee46ec8dc5abe105a1d95c2bace8.jpg', 'Laura Fonces', 'Mestre em business account exterior&nbsp;&nbsp;', '1', '2017-11-27 18:21:39', '2017-11-28 13:58:42', '1', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_publicacoes`
--

CREATE TABLE IF NOT EXISTS `tbl_publicacoes` (
  `id_publicacao` int(10) NOT NULL AUTO_INCREMENT,
  `imagem_publicacao` text,
  `titulo_publicacao` varchar(255) DEFAULT NULL,
  `link_publicacao` varchar(255) DEFAULT NULL,
  `descricao_publicacao` text,
  `cadastrado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `atualizado` datetime DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `excluido` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_publicacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `tbl_publicacoes`
--

INSERT INTO `tbl_publicacoes` (`id_publicacao`, `imagem_publicacao`, `titulo_publicacao`, `link_publicacao`, `descricao_publicacao`, `cadastrado`, `atualizado`, `status`, `excluido`) VALUES
(1, 'a0a073d60721aa14420683594be68f155a1da0bc2a08c.png', 'asda', 'sdasdada', '<strong style="margin: 0px; padding: 0px; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem Ipsum</strong><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">&nbsp;é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma&nbsp;</span>', '2017-11-23 14:32:51', '2017-11-28 14:45:32', '1', 0),
(2, '8ab12d589ceb245ad081cbe9a431e3945a1da0b24d7ac.png', 'qweqweqweq', 'eqweqweqwe', '<strong style="margin: 0px; padding: 0px; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem Ipsum</strong><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">&nbsp;é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma.</span>', '2017-11-23 16:59:05', '2017-11-28 14:45:22', '1', 0),
(3, '8ab12d589ceb245ad081cbe9a431e3945a1da0c60e2c9.png', 'sdcsada', 'adasdasdas', '<strong style="margin: 0px; padding: 0px; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem Ipsum</strong><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">&nbsp;é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma&nbsp;</span>', '2017-11-28 17:45:42', '2017-11-28 14:45:42', '1', 0),
(4, 'a0a073d60721aa14420683594be68f155a1da0d04f060.png', 'asasd', 'adasd', '<strong style="margin: 0px; padding: 0px; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem Ipsum</strong><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">&nbsp;é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma&nbsp;</span>', '2017-11-28 17:45:52', '2017-11-28 14:46:13', '1', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_usuarios`
--

CREATE TABLE IF NOT EXISTS `tbl_usuarios` (
  `id_usuario` int(10) NOT NULL AUTO_INCREMENT,
  `imagem_usuario` text NOT NULL,
  `nome_usuario` varchar(255) NOT NULL,
  `email_usuario` varchar(255) NOT NULL,
  `senha_usuario` varchar(255) NOT NULL,
  `cadastrado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `atualizado` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `excluido` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `tbl_usuarios`
--

INSERT INTO `tbl_usuarios` (`id_usuario`, `imagem_usuario`, `nome_usuario`, `email_usuario`, `senha_usuario`, `cadastrado`, `atualizado`, `status`, `excluido`) VALUES
(3, '4cc4b036737170bd9eb963d24f549a315a1dc76f36865.png', 'funorte', 'adm@funorte.com.br', '@Dmfunorte', '2017-11-23 19:54:22', '2017-11-28 17:30:39', 1, 0),
(4, '2de40e0d504f583cda7465979f958a985a17292e00abe.jpg', 'Root', 'root@stoledo.com.br', '123', '2017-11-23 20:01:50', '2017-11-23 17:01:50', 1, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
