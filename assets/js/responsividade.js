/* 
 * AQUI TERÁ ALGUNS RECURSOS A MAIS PARA AJUDAR NA RESPONSIVIDADE DAS PÁGINAS
 */

//AJUSTE PARA WHATSAPP
$(function () {
    //PEGA A LARGURA DA JANELA
    var janelaw = $(window).width();

    if (janelaw >= 500) {
        $('#whats').css('display', 'none');
    }
});

//AJUSTANDO A RESPONSIVIDADE DA SESSÃO GALERIA
$(function () {
    //PEGA A LARGURA DA JANELA
    var janelaw = $(window).width();

    if (janelaw >= 1720) {
        $('#galeria').css('margin-left', '110px');
    }

    if (janelaw <= 1600 && janelaw >= 1300) {
        $('#galeria').css('margin-left', '70px');
    }

    if (janelaw <= 1366 && janelaw >= 720) {
        $('#galeria').css('margin-left', '200px');
    }
});

