<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Cliente_m extends CI_Model
	{
		// ATRIBUTOS PRIVADOS
		private $nome;
		private $cpf;
		private $nascimento;
		private $sexo;
		private $status;
		private $logradouro;
		private $numero;
		private $bairro;
		private $cidade;
		private $estado;
		private $pais;
		private $cep;
		private $celular;
		private $telefone;
		private $email;
		private $obs;
		private $data_atualizacao;


		/**
		 * Cliente_m constructor.
		 */
		function __construct()
		{
			parent::__construct();

			// SETA A DATA E HORA DO BRASIL
			date_default_timezone_set('America/Recife');

			// CARREGANDO A BIBLIOTECA COM OS COMANDOS SQL
			$this->load->library('MY_Geral_Model');
		}


		/**
		 * @function select simples
		 * @param array $params (tabela,coluna_ordenada,tipo_ordenacao, limite, status, excluido)
		 * @return bool
		 */
		public function select()
		{
			/** @array $dados */
			$dados = array(
				'tabela'          => 'tbl_clientes',
				'coluna_ordenada' => 'id_cliente',
				'tipo_ordenacao'  => 'desc',
				//'limite'          => '1',
				//'status'          => '1',
				'excluido'        => '0'
			);

			$result = $this->my_geral_model->select($dados);

			return (array)$result;
		}


		/**
		 * @function select pelo id
		 * @param array $params (tabela, coluna_id, valor_id, status, excluido)
		 * @return bool
		 */
		public function selectById($id)
		{
			/** @array $dados */
			$dados = array(
				'tabela'    => 'tbl_clientes',
				'coluna_id' => 'id_cliente',
				'valor_id'  => $id,
				'limite'    => '1',
				//'status'    => '1',
				'excluido'  => '0'
			);

			return $this->my_geral_model->selectById($dados);
		}


		/**
		 * @function create
		 * @param array $params (tabela, dados(array))
		 * @return bool
		 */
		public function create()
		{
			/** SETANDO OS VALORES  */
			$this->nome       = $this->input->post('nome_cliente');
			$this->cpf        = $this->input->post('cpf_cliente');
			$this->email      = $this->input->post('email_cliente');
			$this->nascimento = $this->input->post('data_nascimento_cliente');
			$this->sexo       = $this->input->post('sexo_cliente');
			$this->status     = $this->input->post('status');
			$this->logradouro = $this->input->post('logradouro_cliente');
			$this->numero     = $this->input->post('numero_cliente');
			$this->bairro     = $this->input->post('bairro_cliente');
			$this->cidade     = $this->input->post('cidade_cliente');
			$this->estado     = $this->input->post('estado_cliente');
			$this->pais       = $this->input->post('pais_cliente');
			$this->cep        = $this->input->post('cep_cliente');
			$this->celular    = $this->input->post('celular_cliente');
			$this->telefone   = $this->input->post('telefone_cliente');
			$this->obs        = $this->input->post('obs_cliente');

			/** @array $dados */
			$dados = array(
				'tabela' => 'tbl_clientes',
				'dados'  => array(
					'nome_cliente'            => $this->nome,
					'cpf_cliente'             => $this->cpf,
					'email_cliente'           => $this->email,
					'data_nascimento_cliente' => $this->nascimento,
					'sexo_cliente'            => $this->sexo,
					'status'          		  => $this->status,
					'logradouro_cliente'      => $this->logradouro,
					'numero_cliente'          => $this->numero,
					'bairro_cliente'          => $this->bairro,
					'cidade_cliente'          => $this->cidade,
					'estado_cliente'		  => $this->estado,
					'pais_cliente'            => $this->pais,
					'cep_cliente'             => $this->cep,
					'celular_cliente'         => $this->celular,
					'telefone_cliente'        => $this->telefone,
					'obs_cliente'             => $this->obs
				)
			);

			// SE HOVER IMAGEM ADD NO ARRAY DADOS
			if ($_FILES['imagem_cliente']['size'] != 0) {
				$dados['dados'] += ['imagem_cliente' => $this->upload()];
			}

			return $this->my_geral_model->create($dados);
		}


		/**
		 * @function edit
		 * @param array $params (tabela, coluna_id, valor_id, dados(array))
		 * @return bool
		 */
		public function edit($id)
		{
			/** SETANDO OS VALORES  */
			$this->id               = $id;
			$this->nome             = $this->input->post('nome_cliente');
			$this->cpf              = $this->input->post('cpf_cliente');
			$this->email            = $this->input->post('email_cliente');
			$this->nascimento       = $this->input->post('data_nascimento_cliente');
			$this->sexo             = $this->input->post('sexo_cliente');
			$this->status           = $this->input->post('status');
			$this->logradouro       = $this->input->post('logradouro_cliente');
			$this->numero           = $this->input->post('numero_cliente');
			$this->bairro           = $this->input->post('bairro_cliente');
			$this->cidade           = $this->input->post('cidade_cliente');
			$this->estado     	    = $this->input->post('estado_cliente');
			$this->pais             = $this->input->post('pais_cliente');
			$this->cep              = $this->input->post('cep_cliente');
			$this->celular          = $this->input->post('celular_cliente');
			$this->telefone         = $this->input->post('telefone_cliente');
			$this->obs              = $this->input->post('obs_cliente');
			$this->data_atualizacao = date('Y-m-d H:i:s');


			/** @array $dados */
			$dados = array(
				'tabela'    => 'tbl_clientes',
				'coluna_id' => 'id_cliente',
				'valor_id'  => $this->id,
				'dados'     => array(
					'nome_cliente'            => $this->nome,
					'cpf_cliente'             => $this->cpf,
					'email_cliente'           => $this->email,
					'data_nascimento_cliente' => $this->nascimento,
					'sexo_cliente'            => $this->sexo,
					'status'  		          => $this->status,
					'logradouro_cliente'      => $this->logradouro,
					'numero_cliente'          => $this->numero,
					'bairro_cliente'          => $this->bairro,
					'cidade_cliente'          => $this->cidade,
					'estado_cliente'		  => $this->estado,
					'pais_cliente'            => $this->pais,
					'cep_cliente'             => $this->cep,
					'celular_cliente'         => $this->celular,
					'telefone_cliente'        => $this->telefone,
					'obs_cliente'             => $this->obs,
					'atualizado'              => $this->data_atualizacao
				)
			);

			// SE HOVER IMAGEM ADD NO ARRAY DADOS
			if ($_FILES['imagem_cliente']['size'] != 0) {
				$dados['dados'] += ['imagem_cliente' => $this->upload()];
			}


			return $this->my_geral_model->edit($dados);
		}


		/**
		 * @function remove
		 * @param array $params (tabela, coluna_id, valor_id)
		 * @return bool
		 */
		public function remove($valor_id)
		{
			/** SETANDO O VALOR DO ID  */
			$this->id = $valor_id;

			/** @array $dados */
			$dados = array(
				'tabela'    => 'tbl_clientes',
				'coluna_id' => 'id_cliente',
				'valor_id'  => $this->id
			);

			return $this->my_geral_model->remove($dados);
		}


		/**
		 * @function
		 * @param array $params (tabela, coluna_id, valores_ids(array))
		 * @return bool
		 */
		function removeMultiple($valores_ids)
		{
			/** SETANDO O VALOR DO ID  */
			$this->ids = $valores_ids;

			/** @array $dados */
			$dados = array(
				'tabela'      => 'tbl_clientes',
				'coluna_id'   => 'id_cliente',
				'valores_ids' => $this->ids
			);

			return $this->my_geral_model->removeMultiple($dados);
		}


		/**
		 * @function upload de imagem
		 * @param array $params (nome_imagem, nome_pasta)
		 * @return string
		 */
		public function upload()
		{
			/** @array $dados */
			$dados = array(
				'nome_imagem' => 'imagem_cliente',
				'nome_pasta'  => 'clientes'
			);

			return $this->my_geral_model->upload($dados);

		}


		/**
		 * @function download
		 * @param array $params (caminho_arquivo)
		 * @return null
		 */
		public function download($caminho)
		{
			//$caminho = './assets/uploads/clientes/2ba407b9f3e0e72606244a1501d0c5fc5aea03b419350.jpg';

			/** @array $dados */
			$dados = array(
				'caminho_arquivo' => $caminho
			);

			$this->my_geral_model->download($dados);
		}
	}
