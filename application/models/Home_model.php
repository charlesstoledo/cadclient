<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Home_model extends CI_Model
	{

		//CONSTRUTOR
		function __construct()
		{
			parent::__construct();
		}

		//MÉTODO DE GET
		public function get()
		{
			//SELECIONA OS DADOS DO DB
			//CRIA UMA VAR QUE RECEBE OS DADOS DO BANCO
			$dados['contatos'] = $this->db->get('tbl_contatos')->result();
		}

		//MÉTODO DE SET
		public function set()
		{
			//SALVA OS DADOS DO DB
			//VAR QUE RECEBE O DADO PASSADO NO FORM
			$dados ['nome_contato']     = $this->input->post('nome_contato');
			$dados ['email_contato']    = $this->input->post('email_contato');
			$dados ['fone_contato']     = $this->input->post('fone_contato');
			$dados ['mensagem_contato'] = $this->input->post('mensagem_contato');
			$dados ['status_contato']   = 1;

			//INSERINDO NO BANCO
			$this->db->insert('tbl_contatos', $dados);
		}

		//MÉTODO DE DEL
		public function del($id)
		{
			//DELETA OS DADOS DO DB
			//PEGO O ID PASSADO PELO LINK E COLOCO NA CONDIÇÃO
			$this->db->where('id_contato', $id);
			//EXECUTO O DELETE NA TABELA, DE ACORDO COM O WHERE A CIMA
			$this->db->delete('tbl_contatos');
		}

		//MÉTODO DE UPDATE
		public function update($id)
		{
			//SELECIONA OS DADOS PARA ATUALIZAR NO DB
			//PEGO O ID PASSADO PELO LINK E COLOCO NA CONDIÇÃO
			$this->db->where('id_contato', $id);
			//PEGO NO BANCO OS DADOS QUE FOR REFERENTE AO ID
			$data['con'] = $this->db->get('tbl_contatos')->result();
		}

		//MÉTODO DE UPDATE
		public function confirm_update()
		{
			//PEGO O ID PASSADO NO FORM HIDDEN
			$id = $this->input->post('id_contato');
			//ATUALIZA OS DADOS DO DB
			//COLOCO OS DADOS PASSADO PELO FORM NA VAR, COM O NOME DE ACORDO COM A COLUNA DA TBL
			$data['nome_contato']     = $this->input->post('nome_contato');
			$data['email_contato']    = $this->input->post('email_contato');
			$data['fone_contato']     = $this->input->post('fone_contato');
			$data['mensagem_contato'] = $this->input->post('mensagem_contato');

			//FAÇO A CONDIÇÃO PARA EDIÇÃO DOS DADOS
			$this->db->where('id_contato', $id);
			//EXECUTO O UPDATE DOS DADOS DE ACORDO COM O WHERE A CIMA
			$this->db->update('tbl_contatos', $data);
		}

		//MÉTODO DE UPLOAD
		public function upload()
		{
			//UPLOAD DE DADOS PARA DB
			//DEFINE O PATH ONDE O ARQUIVO SERÁ GRAVADO
			$path = "./assets/uploads/emconstrucao";

			//VERIFICA SE EXISTE
			//SE NÃO EXISTIR CRIAMOS COM PERMISSÃO DE LEITURA E ESCRITA
			if (!is_dir($path)) {
				mkdir($path, 0777, $recursive = true);
			}

			//DEFINIMOS AS CONDIÇÕES PARA UPLOAD
			//DETERMINAMOS O PATH PARA GRAVAR O ARQUIVO
			$configUpload['upload_path'] = $path;
			// DEFINIMOS - ATRAVÉS DA EXTENSÃO OS TIPOS DE ARQUIVOS SUPORTADOS
			$configUpload['allowed_types'] = 'jpg|png|gif'; //jpg|png|gif|pdf|zip|rar|doc|xls
			$configUpload['max_size']      = 5000;

			//PASSAMOS AS CONFIGURAÇÕES PARA A LIBRARY UPLOAD
			$this->upload->initialize($configUpload);

			//VERIFICAMOS SE O UPLOAD DA GALERIA FOI PROCESSADO COM SUCESSO
			if (!$this->upload->do_upload('imagem_em_construcao')) {
				//EM CASO DE ERRO RETORNAMOS OS MESMOS PARA UMA VARIÁVEL E ENVIAMOS PARA A VIEW
				//PEGO O ID PASSADO NO FORM HIDDEN
				$id = $this->input->post('id_em_construcao');

				//PEGO O ID PASSADO PELO LINK E COLOCO NA CONDIÇÃO
				$this->db->where('id_em_construcao', $id);
				//PEGO NO BANCO OS DADOS QUE FOR REFERENTE AO ID
				$data['emconstrucao'] = $this->db->get('tbl_em_construcao')->result();

				//DADOS DA MSG
				$data['type']    = "error";
				$data['sucesso'] = $this->upload->display_errors();

				//CHAMA A VIEW, PASSANDO OS DADOS
				$this->load->view('admin/em_construcao/cad_em_construcao', $data);
			} else {
				//SE CORREU TUDO BEM, RECUPERAMOS OS DADOS DO ARQUIVO
				$data['dadosArquivo'] = $this->upload->data();

				//DEFINIMOS O PATH ORIGINAL DO ARQUIVO
				//PEGA A URL A PARTIR DE ASSETS
				$arquivoPath = 'assets/uploads/emconstrucao' . "/" . $data['dadosArquivo']['file_name'];
				//PEGA OS DADOS DO TITUO E LINK
				$dados['titulo_em_construcao']    = $this->input->post('titulo_em_construcao');
				$dados['subtitulo_em_construcao'] = $this->input->post('subtitulo_em_construcao');
				$dados['imagem_em_construcao']    = $arquivoPath;

				//INSERINDO NO BANCO
				$this->db->insert('tbl_em_construcao', $dados);

				//DADOS DA MSG
				$data['type']    = "success";
				$data['sucesso'] = "Dados Salvos com sucesso!";

				//CHAMA A VIEW, PASSANDO OS DADOS
				$this->load->view('admin/emconstrucao/cad_emconstrucao', $data);
			}
		}

		//MÉTODO DE DOWNLOAD
		public function download($nome, $path)
		{ //recebe o nome do arquivo e o caminho do msm
			//DOWNLOAD DE DADOS DO SERVIDOR / DB
			//SUBSTITUI O TRAÇO PELA BARRA
			str_replace("-", "/", $path);
			//FAZ O DOWNLOAD
			force_download($nome, $path);
		}

		//MÉTODO DE PESQUIZA


	}
