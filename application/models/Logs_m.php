<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Logs_m extends CI_Model
	{

		//CONSTRUTOR
		function __construct()
		{
			parent::__construct();
		}

		//MÉTODO DE GET
		public function get()
		{
			//SELECIONA OS DADOS DO DB
			//CRIA UMA VAR QUE RECEBE OS DADOS DO BANCO
			return $this->db->get('tbl_admin_log')->result();
		}

		//FUNCTION EXIBE TODOS OS BANNERS
		public function show_all_logs()
		{
			//ORDENA
			$this->db->order_by('id', 'desc');
			//PEGA
			$query = $this->db->get('tbl_admin_log');
			//VERIFICA SE TEM ALGO
			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				return false;
			}
		}

		//FUNCTION EXIBE APENAS UM BANNER
		public function show_log()
		{
			//ORDENA
			$this->db->order_by('id', 'desc');
			//CONDIÇÃO
			$this->db->where('status_admin', '1');
			//PEGA APENAS 1
			$query = $this->db->get('tbl_admin_log', 1);
			//VERIFICA SE TEM ALGO
			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				return false;
			}
		}
	}
