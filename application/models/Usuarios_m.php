<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_m extends CI_Model {

    //CONSTRUTOR
    function __construct() {
        parent::__construct();
    }

    //MÉTODO DE GET
    public function get() {
        //SELECIONA OS DADOS DO DB
        //CRIA UMA VAR QUE RECEBE OS DADOS DO BANCO
        return $this->db->get('tbl_usuarios')->result();
    }

    //FUNCTION EXIBE TODOS OS BANNERS
    public function show_all_usuarios() {
        //ORDENA
        $this->db->order_by('id_usuario', 'desc');
        //PEGA
        $query = $this->db->get('tbl_usuarios');
        //VERIFICA SE TEM ALGO
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    
    //FUNCTION EXIBE APENAS UM BANNER
    public function show_usuario() {
        //ORDENA 
        $this->db->order_by('id_usuario', 'desc');
        //CONDIÇÃO
        $this->db->where('status', '1');
        //PEGA APENAS 1
        $query = $this->db->get('tbl_usuarios', 1);
        //VERIFICA SE TEM ALGO
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    //FUNCTION PARA ADD BANNER
    public function add_usuario() {

        //SETA A DATA E HORA DO BRASIL
        date_default_timezone_set('America/Recife');

        //CRIA UM ARRAY COM OS DADOS QUE SERÃO INSERIDOS NO BANCO
        $field = array(
            'imagem_usuario' => $this->upload_image(),
            'nome_usuario'   => $this->input->post('nome_usuario'),
            'nivel_usuario'   => $this->input->post('nivel_usuario'),
            'senha_usuario'  => $this->input->post('senha_usuario'),
            'atualizado'    => date('Y-m-d H:i:s'),
            'status'        => $this->input->post('status')
        );

        //INSERE
        $this->db->insert('tbl_usuarios', $field);
        //VERIFICA SE TEM ALGO
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    //FUNCTION QUE DELETAR BANNER
    function delete_usuario() {
        //PEGA O ID
        $id_usuario = $this->input->get('id_usuario');
        //CONDIÇÃO PASSANDO O ID DO BANNER QUE VAI SER DELETADO
        $this->db->where('id_usuario', $id_usuario);
        //DELETA
        $this->db->delete('tbl_usuarios');
        //VERIFICA QUE DELETOU
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    //FUNCTION QUE DELETA MULTIPLOS
    function deleteMulti($ids){
        //CONDIÇÃO QUE DELETA TODOS QUE CONTENHA O ID PASSADO
        $this->db->where_in('id_usuario', $ids);
        //DELATA
        $this->db->delete('tbl_usuarios');
        //VERIFICA SE DELETOU
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    //FUNCTION QUE EDITA BANNER
    public function edit_usuario() {
        //PEGA O ID DO BANNER
        $id_usuario = $this->input->get('id_usuario');
        //CONDIÇÃO PARA EDITAR
        $this->db->where('id_usuario', $id_usuario);
        //PEGA TODOS OS DADOS BANNER
        $query = $this->db->get('tbl_usuarios');

        //VERIFICA SE TEM ALGO
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    //FUNCTION PARA ATUALIZAR O BANNER NO BD
    public function update_usuario() {
        //PEGA O ID DO BANNER 
        $id_usuario = $this->input->post('id_usuario');
        
        //SETA A DATA E HORA DO BRASIL
        date_default_timezone_set('America/Recife');

        //VERIFICA SE TEM ALGUMA IMAGEM NOVA
        if (!empty($_FILES['imagem_usuario'])) {
            //SE TIVER ESSE É O ARRAY CERTO
            $field = array(
                'imagem_usuario' => $this->upload_image(),
                'nome_usuario'   => $this->input->post('nome_usuario'),
                'nivel_usuario'   => $this->input->post('nivel_usuario'),
                'senha_usuario'  => $this->input->post('senha_usuario'),
                'atualizado'    => date('Y-m-d H:i:s'),
                'status'        => $this->input->post('status')
            );
            //SE NÃO, É ESSE OUTRO ARRAY, SEM IMAGEM
        } else {
            $field = array(
                'nome_usuario'   => $this->input->post('nome_usuario'),
                'nivel_usuario'   => $this->input->post('nivel_usuario'),
                'senha_usuario'  => $this->input->post('senha_usuario'),
                'atualizado'    => date('Y-m-d H:i:s'),
                'status'        => $this->input->post('status')
            );
        }

        //CONDIÇÃO PARA ATUALIZAR
        $this->db->where('id_usuario', $id_usuario);
        //ATUALIZA NO BANCO
        $this->db->update('tbl_usuarios', $field);

        //VERIFICA SE TEM ALGO 
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    //FUNCTION PARA FAZER UPLOAD DE IMAGEM
    public function upload_image() {

        //SE EXIXTIR INPUT FILE COM ESSE NAME
        if (isset($_FILES["imagem_usuario"])) {

            //DEFINE O PATH ONDE O ARQUIVO SERÁ GRAVADO
            $path = "./assets/uploads/usuarios/";

            //VERIFICA SE EXISTE
            //SE NÃO EXISTIR CRIAMOS COM PERMISSÃO DE LEITURA E ESCRITA
            if (!is_dir($path)) {
                mkdir($path, 0644, $recursive = true);
            }
            //PEGA A EXTENÇÃO DA IMAGEM
            $extension = explode('.', $_FILES['imagem_usuario']['name']);

            //VERIFICA A EXTENÇÃO
            if ($extension[1] == "png" || $extension[1] == "jpg" || $extension[1] == "gif") {

                //CRIA UM NOVE NOME PARA A IMAGEM
                $new_name = uniqid(md5($_FILES['imagem_usuario']['name'])) . '.' . $extension[1];
                //DESTINO DA IMAGEM
                $destination = $path . $new_name;
                //SETA O DESTINO DA IMAGEM
                move_uploaded_file($_FILES['imagem_usuario']['tmp_name'], $destination);
                //RETORNA O NOVO NOME DA IMAGEM
                return $new_name;
            }
        }
    }

}
