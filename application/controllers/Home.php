<?php defined('BASEPATH') or exit('No direct script access allowed');

	class Home extends CI_Controller
	{
		//CONSTRUTOR
		public function __construct()
		{
			parent::__construct();
		}

		//FUNCTION PRINCIPAL
		public function index()
		{
			//CARREGA A VIEW
			$this->load->view('index');
		}

	}
