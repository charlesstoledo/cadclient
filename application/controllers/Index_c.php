<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Index_c extends CI_Controller
	{

		//CONSTRUTOR
		function __construct()
		{
			parent::__construct();

		}

		public function index()
		{
			//CARREGA A VIEW
			$this->load->view('index');
		}

		public function authentication()
		{
			$re = $this->reCaptcha();

			if ($re == true) {
				//PEGA OS DADOS ENVIADOS
				$usuario = $this->input->post('usuario');
				$senha   = $this->input->post('senha');

				//FAZ UMA BUSCA NO BANCO PELO USUARIO PASSADO E SENHA
				$this->db->where('nome_usuario', $usuario);
				$this->db->where('senha_usuario', $senha);
				$this->db->where('status', 1);

				//PEGA O USUÁRIO QUE EXISTE NO BANCO
				$usuario = $this->db->get('tbl_usuarios')->result();

				//VERIFICA SE EXISTE 1
				if (count($usuario) === 1) {

					//CRIA UM ARRAY COM O NOME DO USUARIO E DIZ QUE ELE ESTÁ LOGADO
					$dados = array('usuario' => $usuario[0]->nome_usuario,
								   'logado'  => TRUE,
								   'img'     => $usuario[0]->imagem_usuario,
								   'nivel'   => $usuario[0]->nivel_usuario);
					//INICIA UMA SESSÃO COM O ARRAY DE DADOS PASSANDO AS REFERENCIAS
					$this->session->set_userdata($dados);
					//MSG
					//$msg['s'] = "Login realizado com sucesso!";

					//REGISTRANDO NO BANCO O LOGIN DO ADMIN
					$dados_admin['nome_admin']   = $usuario[0]->nome_usuario;
					$dados_admin['ip_admin']     = $_SERVER['REMOTE_ADDR'];
					$dados_admin['status_admin'] = $usuario[0]->status;

					//INSERINDO LOG NO BANCO
					$this->db->insert('tbl_admin_log', $dados_admin);

					//REDIRECIONA
					//redirect(base_url('admin/home'), 'refresh');

					$dados['sucesso'] = TRUE;

					//SE NÃO DÉ CERTO O LOGIN E SENHA
				} else {

					$dados['erro'] = TRUE;
				}
			} else {
				$dados['re_erro'] = TRUE;
			}


			sleep(1);

			echo json_encode($dados);
		}

		//FUNÇÃO PARA SAIR DO SISTEMA
		public function logout()
		{
			//DESTROI A SESSÃO
			$this->session->sess_destroy();
			//REDIRECIONA
			redirect(base_url());
		}


		public function reCaptcha()
		{
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
																	  "secret"   => "6LdYiVQUAAAAAOGhdrLX94IRIl047cAte9W7oGAo",
																	  "response" => $_POST["g-recaptcha-response"],
																	  "remoteip" => $_SERVER["REMOTE_ADDR"]
																  )));

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$recaptch = json_decode(curl_exec($ch), true);

			curl_close($ch);

			if ($recaptch["success"] == true) {
				return true;
			} else {
				return false;
			}
		}
	}
