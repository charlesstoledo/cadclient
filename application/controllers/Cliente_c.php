<?php defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	 * Class Cliente_c
	 */
	class Cliente_c extends CI_Controller
	{
		// CONSTRUTOR
		function __construct()
		{
			parent::__construct();

			// CHAMADA DO MODEL
			$this->load->model('Cliente_m', 'clientes');

		} // END FUNCTION CONSTRUCT

		/**
		 * @FUNCTION PRINCIPAL
		 */
		public function index()
		{
			// CARREGA A VIEW
			$this->load->view('clientes/clientes');
		} // END FUNCTION INDEX

		/**
		 * @FUNCTION SELECT
		 */
		public function select()
		{
			// INICIANDO ARRAY QUE PASSARÁ OS DADOS PARA A VIEW
			$result = array('data' => array());

			// RECEBENDO DADOS DO BANCO
			$data = $this->clientes->select();

			// VERIFICANDO SE EXISTE DADOS VINDO DO BANCO
			if ($data[0] === false) {
				// PASSANDO PARA O ARRAY DATA
				$result['data'] = array();
			} else {
				// FOREACH PARA TRATAR DO VALORES VINDO DO BANCO
				foreach ($data as $key => $value) {
					// CHECKBOX DA LINHA
					$checkbox =
						'<div class="center">
						<label class="pos-rel">
						<input id="checkboxSon" name="excluir_todos[]" value="' . $value['id_cliente'] . '" type="checkbox" class="ace" />
						<span class="lbl"></span>
						</label>
					</div>';

					// ID
					$id = '<div> ' . $value['id_cliente'] . ' </div>';

					$imagem = '<div>
								<a download="" href="' . base_url("assets/uploads/clientes/" . $value['imagem_cliente'] . " ") . ' " >
									<img  src="' . base_url("assets/uploads/clientes/" . $value['imagem_cliente'] . " ") . ' " class="img-responsive" alt="User Image" width="250" height="">
						   	    </a>
						   	   </div>';

					// NOME
					$nome = '<div> ' . $value['nome_cliente'] . ' </div>';

					// CPF
					$cpf = '<div> ' . $value['cpf_cliente'] . ' </div>';

					// CELULAR
					$celular = '<div> ' . $value['celular_cliente'] . ' </div>';

					// EMAIL
					$email = '<div> ' . $value['email_cliente'] . ' </div>';

					// STATUS
					switch ($value['status']) {
						case 1:
							$status = '<span class="label label-sm label-success"> Ativo </span>';
							break;

						default:
							$status = '<span class="label label-sm label-warning"> Inativo </span>';
							break;
					}

					// BOTÕES DE AÇÃO
					$buttons =
						'<div class="action-buttons">
						<a class="green" href="javascript:;" >
							<i class="ace-icon fa fa-eye bigger-130 item-edit" onclick="editMember(' . $value['id_cliente'] . ')" data-toggle="modal" data-target="#myModal" title="Visualizar"></i>
						</a>
						<a class="red" href="javascript:;" >
							<i class="ace-icon fa fa-trash-o bigger-130 item-delete" onclick="removeMember(' . $value['id_cliente'] . ')" data-toggle="modal" data-target="#removeMemberModal" title="Excluir"></i>
						</a>
						<a class="blue" download="" href="' . base_url("assets/uploads/clientes/" . $value['imagem_cliente'] . " ") . ' " >
							<i class="ace-icon fa fa-download bigger-130 item-edit" title="Baixar"></i>
						</a>
					</div>';

					// FORMATANDO DATA DA ATUALIZAÇÃO
					$atualizado = date("d-m-Y H:i:s", strtotime($value['atualizado']));

					// PASSANDO VALOR PARA O ARRAY DATA
					$result['data'][$key] = array(
						$checkbox,
						$id,
						$imagem,
						$nome,
						$cpf,
						$celular,
						$email,
						$atualizado,
						$status,
						$buttons
					);
				} // END FOREACH
			}


			// PASSADO RESULTADO PARA VIEW
			echo json_encode($result);

		} // END FUNCTION SELECT


		/**
		 * @FUNCTION CREATE
		 */
		public function create()
		{
			// VALIDAÇÃO DOS CAMPOS
			$config = array(
				array(
					'field' => 'nome_cliente',
					'label' => 'Nome do Cliente',
					'rules' => 'trim|required')
			);

			// SETAS A VALIDAÇÃO
			$this->form_validation->set_rules($config);

			// VERIFICA SE A VALIDAÇÃO FOI REALIZADA COM SUCESSO
			if ($this->form_validation->run() === true) {
				// ENCAMINHA PARA O MODEL PARA INSERIR NO BANCO
				$createMember = $this->clientes->create();
				// SUCESSO TRUE
				$validator['success'] = $createMember;
			} else {
				// SUCESSO FALSE
				$validator['success'] = false;
			}

			// RETORNA O SUCESSO
			echo json_encode($validator);
		} // END FUNCTION CREATE


		/**
		 * @FUNCTION SELECBYID
		 * @PARAMETRO ID
		 */
		public function selectById($id)
		{
			// VERIFICA SE EXISTE ID
			if ($id) {
				// ENVIA PARA O MÉTODO NO MODEL PASSANDO O ID
				$data = $this->clientes->selectById($id);
				// RETORNA A CONSULTA
				echo json_encode($data);
			} // END IF

		} // END FUNCTION SELECBYID


		/**
		 * @FUNCTION EDIT
		 * @PARAMETRO ID
		 */
		public function edit($id = null)
		{
			// VERIFICA SE EXISTE ID
			if ($id) {

				// VALIDAÇÃO DOS CAMPOS
				$config = array(
					array(
						'field' => 'nome_cliente',
						'label' => 'Nome do Cliente',
						'rules' => 'trim|required')
				);

				// SETA A VALIDAÇÃO
				$this->form_validation->set_rules($config);

				// VERIFICA SE A VALIDAÇÃO FOI REALIZADA COM SUCESSO
				if ($this->form_validation->run() === true) {
					// ENCAMINHA PARA O MODEL PASSANDO O ID
					$editMember = $this->clientes->edit($id);

					$validator['success'] = $editMember;

				} else {
					// SUCCESS RECEBE FALSE
					$validator['success'] = false;
				} // END ELSE

				// RETORNA SUCCESS
				echo json_encode($validator);
			} // END IF

		} // END FUNCTION EDITAR


		/**
		 * @FUNCTION REMOVE
		 * @PARAMETRO ID
		 */
		public function remove($id = null)
		{
			// VERIFICA SE EXISTE ID
			if ($id) {
				// PASSANDO ID PARA O MODEL EXCLUIR DO BANCO
				$removeMember = $this->clientes->remove($id);
				// SUCCESS RECEBE O RETORNO DO MODEL
				$validator['success'] = $removeMember;
				// RETORNA O SUCCESS
				echo json_encode($validator);
			} // END IF

		} // END FUNCTION REMOVE


		/**
		 * @FUNCTION REMOVEMLUTIPLE
		 */
		public function removeMultiple()
		{
			// PEGA OS IDS PASSADOS PELO INPUT
			$ids            = $this->input->post("excluir_todos");
			// FAZ UMA CONTAGEM DOS IDS
			$numRegs        = count($ids);
			// INICIA O SUCESSO COMO FALSE
			$msg['success'] = false;

			// VERIFICA SE A CONTAGEM DE IDS É MAIOR QUE ZERO
			if ($numRegs > 0) {
				// ENVIA OS IDS PARA SEREM REMOVIDOS
				$this->clientes->removeMultiple($ids);

				// SUCESSO RECEBE TRUE
				$msg['success'] = true;

				// RETORNA A MSG PARA A VIEW
				echo json_encode($msg);

			} else {
				// RETORNA A MSG PARA A VIEW
				echo json_encode($msg);
			}
		}// END FUNCTION REMOVEMULTIPLE


	} // END CLASS CLIENTE_C
