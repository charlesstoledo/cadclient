<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Logs_c extends CI_Controller
	{

		//CONSTRUTOR
		function __construct()
		{
			parent::__construct();
			//CAL MODEL BANNER_M
			$this->load->model('Logs_m', 'm');
		}

		//PAGE BANNERS
		public function index()
		{
			//CARREGA A VIEW
			$this->load->view('logs/logs');
		}

		public function show_all_logs()
		{
			$result = $this->m->show_all_logs();

			echo json_encode($result);
		}


	}
