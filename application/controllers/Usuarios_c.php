<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Usuarios_c extends CI_Controller
	{
		//CONSTRUTOR
		function __construct()
		{
			parent::__construct();
			//CAL MODEL BANNER_M
			$this->load->model('Usuarios_m', 'm');
		}

		//PAGE BANNERS
		public function index()
		{
			//CARREGA A VIEW
			$this->load->view('usuarios/usuarios');
		}

		public function show_all_usuarios()
		{
			$result = $this->m->show_all_usuarios();

			echo json_encode($result);
		}

		//PAGE ADD BANNER
		public function add_usuario()
		{
			//CARREGA A VIEW

			$result         = $this->m->add_usuario();
			$msg['success'] = false;
			$msg['type']    = 'add';

			if ($result) {
				$msg['success'] = true;
			}

			echo json_encode($msg);
		}

		public function delete_usuario()
		{
			$result = $this->m->delete_usuario();

			$msg['success'] = false;
			if ($result) {
				$msg['success'] = true;
			}
			echo json_encode($msg);
		}

		public function excluirMulti()
		{
			$ids            = $this->input->post("excluir_todos");
			$numRegs        = count($ids);
			$msg['success'] = false;

			if ($numRegs > 0) {
				$this->m->deleteMulti($ids);
				if ($ids) {
					$msg['success'] = true;
				}
				echo json_encode($msg);

			} else {
				echo json_encode($msg);
			}
		}

		public function edit_usuario()
		{
			$result = $this->m->edit_usuario();
			echo json_encode($result);
		}

		public function update_usuario()
		{
			$result         = $this->m->update_usuario();
			$msg['success'] = false;
			$msg['type']    = 'update';
			if ($result) {
				$msg['success'] = true;
			}
			echo json_encode($msg);
		}

	}
