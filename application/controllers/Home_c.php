<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Home_c extends CI_Controller
	{

		//CONSTRUTOR
		function __construct()
		{
			parent::__construct();

			$this->load->model('Usuarios_m', 'U');
			$this->load->model('Logs_m', 'L');

		}

		public function index()
		{

			$dados['usuarios'] = $this->U->get();
			$dados['logs']     = $this->L->get();

			//CARREGA A VIEW
			$this->load->view('home', $dados);
		}


	}
