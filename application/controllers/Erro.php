<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Erro extends CI_Controller {

    //CONSTRUTOR
    function __construct() {
        parent::__construct();
    }

    public function index() {
        
        //CARREGA A VIEW
        $this->load->view('404');
    }
}