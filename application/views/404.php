<!doctype html>
<html lang="pt-br">
    <head>
        <!-- important for compatibility charset -->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>404 | CadClient</title>

        <meta name="author" content="">
        <meta name="keywords" content="">
        <meta name="description" content="">

        <!-- important for responsiveness remove to make your site non responsive. -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- FavIcon -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/images/favicon.png">

        <!--INCLUINDO O CSS-->
        <?php include_once VIEWPATH . 'other_includes/css.php'; ?>

    </head>

    <body>
        <!-- Page Preloader -->
        <div id="loading">
            <div id="loading-center">
                <div id="loading-center-absolute">
                    <div id="object"></div>
                </div>
            </div>
        </div>
        <!-- Page Preloader Ends /-->

        <!-- Main Container -->
        <div class="main-container">

            <!--INCLUINDO TOP BAR START-->
            <?php include_once VIEWPATH . 'other_includes/top_bar.php'; ?>

            <!-- Header Starts -->
            <div class="header header-new">
                <div class="row">

                    <div class="medium-4 small-12 columns">
                        <div class="logo">
                            <a href="<?php echo base_url("admin"); ?>">
                                <img style="" src="<?php echo base_url(); ?>assets/images/NewLogo.png" alt="" />
                            </a>    
                        </div><!-- logo /-->
                    </div><!-- left Ends /-->

                </div><!-- Row Ends /-->
            </div>
            <!-- Header Ends /-->

            <!-- Title Section -->
            <div class="title-section module">
                <div class="row">

                    <div class="small-12 columns">
                        <h1>404 Error!</h1>
                    </div><!-- Top Row /-->

                    <div class="small-12 columns">
                        <ul class="breadcrumbs">
                            <li><a href="<?php echo base_url(); ?>">Home</a></li>
                            <li><span class="show-for-sr">Current: </span> Erro</li>
                        </ul><!-- Breadcrumbs /-->
                    </div><!-- Bottom Row /-->

                </div><!-- Row /-->
            </div>
            <!-- Title Section Ends /-->

            <!-- Content section -->
            <div class="content-section module pageerror">

                <div class="row">
                    <h2>404!</h2>
                    <h3>Página não encontrada!</h3>

                    <div class="clearfix"></div>

                    <div class="medium-6 columns error-page-form">
                        <div class="input-group">
                            <div class="input-group-button">
                                <a style="padding: 20px" class="button primary" href="<?php echo base_url(); ?>">Voltar para a página Home!</a>
                            </div>
                        </div>
                    </div> 

                </div><!-- Row Ends /-->

            </div>
            <!-- Content Section Ends /-->

            <!--INCLUINDO FOOTER-->
            <?php //include_once VIEWPATH . '_includes/footer.php'; ?>


            <!-- ---------------- FOOTER DO SITE ---------------- -->


        </div>
        <!-- Main Container /-->

        <a href="#top" id="top" class="animated fadeInUp start-anim"><i class="fa fa-angle-up"></i></a>

        <!--INCLUINDO O js-->
        <?php include_once VIEWPATH . 'other_includes/js.php'; ?>

    </body>
</html>
