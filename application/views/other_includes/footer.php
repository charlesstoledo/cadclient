
<!-- ---------------- FOOTER DO SITE ---------------- -->

<!-- Footer -->
<div class="footer">
    <div class="footerTop">

        <div class="row">

            <div class="large-6 medium-6 small-12 columns footer-widget">
                <h2>Institucional</h2>
                <div class="tx-div"></div>

            </div><!-- Widget 1 ends /-->

            <div class="large-6 medium-6 small-12 columns footer-widget">
                <div class="textwidget">
                    <ul class="address">

                        <?php
                        //VERIFICA SE EXISTE CONTATO
                        if (isset($contatos) && $contatos != NULL) {
                            //LISTA OS CONTATOS
                            foreach ($contatos as $c) {
                                ?>

                                <li>
                                    <i class="fa fa-home"></i>
                                    <h4>Endereço:</h4>
                                    <br />
                                    <p><?php echo $c->endereco_contato; ?></p>
                                </li>
                                <li>
                                    <i class="fa fa-mobile"></i>
                                    <h4>Fones:</h4>
                                    <p><?php echo $c->fixo_contato; ?> / <?php echo $c->celular_contato; ?></p>
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    <h4>E-mail:</h4>
                                    <p><?php echo $c->email_contato; ?></p>
                                </li>
                            </ul>
                            <hr>
                            <div class="socialicons">
                                Social: 
                                <a href="<?php echo $c->facebook_contato; ?>"><i class="fa fa-facebook"></i></a>
                                <a href="<?php echo $c->twitter_contato; ?>"><i class="fa fa-twitter"></i></a>
                                <a href="<?php echo $c->instagram_contato; ?>"><i class="fa fa-google"></i></a>
                            </div><!-- Social Icons /-->

                            <?php
                        }
                    }
                    ?>
                </div><!-- text widget /-->
            </div><!-- widget 4 /-->                
            <div class="clearfix"></div>

        </div><!-- Row Ends /-->

    </div><!-- footerTop Ends here.. -->

    <div class="footerbottom">

        <div class="row">

            <div class="medium-12 small-12 columns">
                <div style="text-align: center" class="copyrightinfo">2018 &copy; CadClient</div>
            </div><!-- left side /-->

        </div><!-- Row /-->

    </div><!-- footer Bottom /-->
</div>
<!-- Footer Ends here /-->

