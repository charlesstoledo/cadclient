

<!-- ---------------- HEADER DO SITE ---------------- -->

<!-- Header Starts -->
<div class="header header-new">
    <div class="row">

        <div class="medium-4 small-12 columns">
            <div class="logo">
                <a href="<?php echo base_url(); ?>">
                    <img style="height: 100px" src="<?php echo base_url(); ?>assets/images/NewLogo.png" alt="" />
                </a>    
            </div><!-- logo /-->
        </div><!-- left Ends /-->

    </div><!-- Row Ends /-->
</div>
<!-- Header Ends /-->

<!-- Navigation Wrapper -->
<div class="navigation-style-two">
    <div class="row nav-wrap">
        <!-- navigation Code STarts here.. -->
        <div class="top-bar">
            <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                    <a  data-toggle><span style="margin-left: 2px" class=" menu-icon dark float-left"></span></a>
                    <br /><b style="margin-left: -10px">Menu</b>
                </span>
            </div>

            <nav style="" id="responsive-menu">
                <ul class="menu vertical medium-horizontal float-right" data-responsive-menu="accordion medium-dropdown">
                    <li class="single-sub parent-nav">
                        <a href="<?php echo base_url(); ?>">Home</a>
                    </li>
                    <li class="single-sub parent-nav"><a href="#"> Sobre nós </a>
                        <ul class="child-nav menu vertical">
                            <li>
                                <a href="<?php echo base_url(); ?>institucional">Institucional</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>galeria">Galeria</a>
                            </li>                                         
                        </ul>
                    </li>

                    <li class="single-sub parent-nav"><a href="#"> Cursos </a>
                        <ul class="child-nav menu vertical">
                            <li>
                                <a href="<?php echo base_url(); ?>especializacao">Especialização</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>aperfeicoamento">Aperfeiçoamento</a>
                            </li> 
                            <li>
                                <a href="<?php echo base_url(); ?>outros">Outros</a>
                            </li> 
                        </ul>
                    </li>

                    <li class="single-sub parent-nav">
                        <a href="<?php echo base_url(); ?>noticias">Notícias</a>
                    </li>
                    <li class="single-sub parent-nav">
                        <a href="<?php echo base_url(); ?>biblioteca">Biblioteca</a>
                    </li>
                    <li class="single-sub parent-nav">
                        <a href="<?php echo base_url(); ?>inscricoes">Inscrições</a>
                    </li>
                    <li class="single-sub parent-nav">
                        <a href="#">Fale conosco</a>
                        <ul class="child-nav menu vertical">
                            <li><a href="<?php echo base_url(); ?>contato">Contato</a></li>
                            <li><a href="<?php echo base_url(); ?>cadastro_pacientes">Cadastro de paciêntes</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div><!-- top-bar Ends -->
        <!-- Navigation Code Ends here -->
        <!--<div class="search-wrap float-right">
            <a href="<?php //echo base_url();   ?>assets/#" class="search-icon-toggle" data-toggle="search-dropdown"><i class="fa fa-search"></i></a>
        </div>--><!-- search wrap ends -->
        <div class="dropdown-pane" id="search-dropdown" data-dropdown data-auto-focus="true">
            <input type="text" placeholder="Digite uma palavra e aperte enter .... " />
        </div>
    </div><!-- right Ends /-->
</div>
<!-- Navigation Wrapper Ends /-->
