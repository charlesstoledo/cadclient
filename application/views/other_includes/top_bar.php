
<!-- ---------------- TOP BAR DO SITE ---------------- -->

<!-- Top Bar Starts -->
<div class="topBar">
    <div class="row">

        <div class="large-6 medium-6 small-12 columns left-side"></div> 

        <div class="large-6 medium-6 small-12 columns">
            <ul class="menu text-right">

                <?php
                //VERIFICA SE EXISTE CONTATO
                if (isset($contatos) && $contatos != NULL) {
                    //LISTA OS CONTATOS
                    foreach ($contatos as $c) {
                        ?>

                        <li>
                            <i class="fa fa-envelope"></i>
                        </li>
                        <li class="first-social social"><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li class="social"><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li class="social"><a href=""><i class="fa fa-instagram"></i></a></li>

                        <?php
                    }
                }
                ?>
            </ul>
        </div><!-- Right column Ends /-->

    </div><!-- Row ends /-->
</div>
<!-- Top bar Ends /-->
