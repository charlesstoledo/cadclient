    
    <!-- ---------------- JS DO SITE ---------------- -->

    <!-- Including Jquery so All js Can run -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.12.2.js"></script>

    <!-- Including Foundation JS so Foundation function can work. -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/foundation.min.js"></script>

    <!-- Including Owl Carousel File -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>

    <!-- Webful JS -->
    <script src="<?php echo base_url(); ?>assets/js/webful.js"></script>

    <!-- Including LightBox Plugin Delete if not using -->
    <script src="<?php echo base_url(); ?>assets/js/lightbox.min.js"></script>

    <!-- REVOLUTION JS FILES Delete if Not Using Revolution Slider -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/revolution/js/jquery.themepunch.revolution.min.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
        (Load Extensions only on Local File Systems ! The following part can be removed on Server for On Demand Loading) -->	
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/revolution/js/extensions/revolution.extension.video.min.js"></script>
    
    <!-- Including Script to Responsividade Creat by Charles  -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/responsividade.js"></script>