<!--BOTÃO DE STATUS-->
<script type="text/javascript">
	if (!$('html').hasClass('lte9')) {
		var Switch     = require('ios7-switch')
			, checkbox = document.querySelector('.ios')
			, mySwitch = new Switch(checkbox);
		mySwitch.toggle();
		mySwitch.el.addEventListener('click', function (e) {
			e.preventDefault();
			mySwitch.toggle();
		}, false);
		//creating multiple instances
		var Switch2     = require('ios7-switch')
			, checkbox  = document.querySelector('.iosblue')
			, mySwitch2 = new Switch2(checkbox);

		mySwitch2.el.addEventListener('click', function (e) {
			e.preventDefault();
			mySwitch2.toggle();
		}, false);
	}
</script>

<script type="text/javascript">

	// VARIÁVEL GLOBAL DA DATATABLE
	var manageMemberTable;

	// FUNÇÃO AUTOMÁTIZADA APÓS CARREGAMENTO DA PÁGINA
	$(document).ready(function () {

		// INICIANDO O DATATABLE
		manageMemberTable = $("#dynamic-table").DataTable({
			// AJAX RECEBENDO OS DADOS ENVIADOS PELO CAMINHO/CONTROLE/MÉTODO
			'ajax'     : '<?php echo base_url() ?>cliente_c/select',
			// ORDENAÇÃO AUTOMÁTICA DA TABELA PELA 2 COLUNA DECRESSENTE
			order      : [[1, 'desc']],
			responsive : true,
			//dom: 'Bfrtip',
			//buttons: [
			// 'copy', 'csv', 'excel', 'pdf', 'print', 'colvis'
			//],
			// TRADUÇÃO
			"language" :
				{
					"sEmptyTable"    : "Nenhum registro encontrado",
					"sInfo"          : "Mostrando de _START_ até _END_ de _TOTAL_ registros",
					"sInfoEmpty"     : "Mostrando 0 até 0 de 0 registros",
					"sInfoFiltered"  : "(Filtrados de _MAX_ registros)",
					"sInfoPostFix"   : "",
					"sInfoThousands" : ".",
					"sLengthMenu"    : "_MENU_ resultados por página",
					"sLoadingRecords": "Carregando...",
					"sProcessing"    : "Processando...",
					"sZeroRecords"   : "Nenhum registro encontrado",
					"sSearch"        : "Pesquisar",

					"lengthMenu"  : "Mostrando _MENU_ registros por página",
					"zeroRecords" : "Nenhum arquivo encontrado",
					"info"        : "Mostrando Página _PAGE_ de _PAGES_",
					"infoEmpty"   : "Nenhum registro disponível",
					"infoFiltered": "(filtro de _MAX_ registros no total)",
					"search"      : "Procurar:",

					'oPaginate':
						{
							'sNext'    : 'Próximo',
							'sPrevious': 'Anterior',
							'sFirst'   : 'Primeiro',
							'sLast'    : 'Último'
						},
					'oAria'    :
						{
							'sSortAscending' : ': Ordenar colunas de forma ascendente',
							'sSortDescending': ': Ordenar colunas de forma descendente'
						}

				},
			// ESTILO DO SELECT DA LINHA
			select     : {
				style: 'multi'
			},
			//COLUNAS DA TABELA QUE PODEM SER ORDENADAS
			"aoColumns": [
				{"bSortable": false},       // COLUNA  DOS CHECKBOXES - NÃO ORDENA
				null, null, null, null, null, null, null, null, // COLUNAS QUE PODEM SER ORDENADAS
				{"bSortable": false}]      // COLUNA  DE AÇÕES - NÃO ORDENA
			//scrollY: 600,
		}); // END

		/** BOTÕES QUE FICAM EM CIMA DA TABELA */
		$.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';

		new $.fn.dataTable.Buttons(manageMemberTable, {
			buttons: [
				{
					"extend"   : "colvis",
					"text"     : "<i class='fa fa-search bigger-110 blue' title='Ocultar colunas'></i> <span class='hidden'>Ocutar/Aparecer colunas</span>",
					"className": "btn btn-white btn-primary btn-bold"
					//columns: ':not(:first):not(:last)'
				},
				{
					"extend"   : "print", //buttons.print.min.js
					"text"     : "<i class='fa fa-print bigger-110 grey jq_print' title='Imprimir'></i> <span class='hidden'>Imprimir</span>",
					"className": "btn btn-white btn-primary btn-bold"
				},
				{
					"extend"   : "copy",
					"text"     : "<i class='fa fa-copy bigger-110 pink' title='Copiar tabela'></i> <span class='hidden'>Copiado para transferência</span>",
					"className": "btn btn-white btn-primary btn-bold"
				},
				{
					"extend"   : "csv",
					"text"     : "<i class='fa fa-database bigger-110 orange' title='Baixar csv'></i> <span class='hidden'>Exportar para CSV</span>",
					"className": "btn btn-white btn-primary btn-bold"
				},
				{
					"extend"   : "excel",
					"text"     : "<i class='fa fa-file-excel-o bigger-110 green' title='Baixar excel'></i> <span class='hidden'>Exportar para Excel</span>",
					"className": "btn btn-white btn-primary btn-bold"
				},
				{
					"extend"   : "pdf",
					"text"     : "<i class='fa fa-file-pdf-o bigger-110 red' title='Baixar pdf'></i> <span class='hidden'>Exportar para PDF</span>",
					"className": "btn btn-white btn-primary btn-bold"
				}
			]
		});

		manageMemberTable.buttons().container().appendTo($('.tableTools-container'));


		/** SELECIONAR E DESELECIONAR LINHAS DA TABELA */

		// INICIA O CHECKBOX DO THEAD DA TABELA COMO FALSE(DESMARCADO)
		$('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);

		// SELECIONA/DESELECIONA TODAS AS LINHAS DE ACORDO COM O CHECKBOX PRINCIPAL, O DO THEAD DA TABELA
		$('#dynamic-table > thead > tr > th input[type=checkbox], #dynamic-table_wrapper input[type=checkbox]').eq(0).on('click', function () {
			// PEGA O ATRIBUTO CHECKED DO INPUT DO THEAD
			var th_checked = this.checked;
			// PROCURA POR TODAS AS LINAS DA TABELA
			$('#dynamic-table').find('tbody > tr').each(function () {
				// PEGA AS LINHAS
				var row = this;
				// VERIFICA SE O INPUT DO THEAD É VERDADEIRO
				if (th_checked) {
					// MARCA TODAS AS LINHAS COM A CLASSE SELECTED
					manageMemberTable.row(row).select();
					// MARCA TODOS OS CHECKBOXES DAS LINHAS
					$('#checkboxSon*').prop('checked', true);
				} else {
					// DESMARCA TODAS AS LINHAS COM A CLASSE SELECTED
					manageMemberTable.row(row).deselect();
					// DESMARCA TODOS OS CHECKBOXES DAS LINHAS
					$('#checkboxSon*').prop('checked', false);
				}
			});
		}); // END


		// SELECIONA/DESELECIONA AS LINHAS QUANDO CLICA NO CHECKBOX DA PRÓPRIA LINHA
		$('#dynamic-table').on('click', 'td input[type=checkbox]', function () {
			// PEGA A LINHA DO CHECKBOX CLICADO
			var row = $(this).closest('tr').get(0);
			// VERIFICA SE ESTÁ COM A PROPRIEDADE CHECKED
			if (this.checked) {
				// SE SIM, DESMARCA A LINHA QUE ESTIVER COM A CLASS SELECTED
				manageMemberTable.row(row).deselect();
			} else {
				// SE NÃO, MARCA A LINHA COM A CLASS SELECTED
				manageMemberTable.row(row).select();
			}
		}); // END


		// SELECIONA/DESELECIONA AS LINAS QUANDO CLICADO NA PRÓPRIA LINA (TR) E MARCA O CHECKBOX
		$('#dynamic-table tbody').on('click', 'tr', function () {
			// FAZ ESSE SETTIMEOUT DE 100 MILÉSIMOS SÓ PARA DAR TEMPO DE PEGAR A CLASSE ATUALIZADA
			setTimeout(() => {
				// PEGA A CLASSE QUE A LINHA ESTÁ
				var select = $(this).attr("class");
				// VERIFICA QUAL A CLASS DA LINHA
				if (select == "odd selected" || select == "even selected") {
					// MARCA O CHECKBOX DA LINHA
					$(this).find('td div label input').prop('checked', true);
				} else {
					// DESMARCA O CHECKBOX DA LINHA
					$(this).find('td div label input').prop('checked', false);
				}
			}, 100);

		}); // END


		//ATUALIZANDO EM TEMPO REAL O TEXTO DO STATUS E O VALOR
		$('.ios-switch').click(function () {
			if ($(this).hasClass('on')) {

				$(this).next('input:checkbox').attr('checked', true).val('1');
				$('.stt').html('Ativo');
			} else {

				$(this).next('input:checkbox').removeAttr('checked').val('0');
				$('.stt').html('Inativo');
			}
		});

		//ATUALIZANDO IMAGEM ATUAL EM TEMPO REAL
		$('input[name=imagem_cliente]').on('change', function () {
			var src    = document.getElementById("id-input-file-2").files[0];
			var imagem = window.URL.createObjectURL(src);
			$('.aqui').attr("src", imagem);
		});

	}); // END DOCUMENTO.READY


	/** FUNCÃO PARA ADICIONAR */
	function addMemberModel() {

		$('#img_atual*').hide();

		//INICIA O EDITOR DE TEXTO SUMMERNOTE EM BRANCO
		$('#summernote').summernote('code', '');

		// SIMULA O EVENTO QUE LIMPA O CAMPO DE ESCOLHER IMAGEM
		$('.remove').click();

		// SUBSTITUI O TÍTULO DO MODAL
		$('#myModal').find('.modal-title').text('Add Novo Cliente');
		// ADICIONA O CAMINHO DA AÇÃO DO FORM
		$('#myForm').attr('action', '<?php echo base_url() ?>cliente_c/create');
		// LIMPA O FORM
		$("#myForm")[0].reset();

		$('.ios-switch').removeClass('off').addClass('on').next('input:checkbox').attr('checked', true).val('1');


		// SUBMIT DO FORMULÁRIO
		$("#myForm").unbind('submit').bind('submit', function () {

			if ($("input[name=nome_cliente]").val() == "") {
				alert("Coloque seu nome!");

			} else {

				var obs = $('input[name=obs_cliente]');

				//PEGA OS VALORES EXATOS DO EDITOR
				var markupStr = $('#summernote').summernote('code');
				//SETA NO VALOR DA VARIÁVEL TITULO
				obs.val(markupStr);

				// PEGA O FORMULÁRIO
				var form = $(this);

				// PEGA O FORMULÁRIO
				var formData = new FormData(this);

				// CHAMA O AJAX
				$.ajax({
					url        : form.attr('action'), // PEGA O ACTION DO FORM
					type       : form.attr('method'),// PEGA O MÉTODO DO FORM
					//data    : form.serialize(),   // SERIALIZA OS DADOS DO FORM PARA ENVIAR AO CONTROLE --> NÃO ENVIA IMAGEM
					data       : formData, // PARA ENVIAR DADOS E IMAGENS
					processData: false,
					contentType: false,
					dataType   : 'json',
					// RETORNO DE SUCESSO
					success    : function (response) {
						// VERIFICA SE A RESPOSTA É VERDADEIRA
						if (response.success === true) {
							// MENSAGEM DE SUCESSO
							sucesso('Sucesso!', 'Cliente adicionado.');
							// OCULTA O MODAL
							$("#myModal").modal('hide');
							// RECARREGA A TABELA
							manageMemberTable.ajax.reload(null, false);
						} else {
							// MENSAGEM DE ATENÇÃO
							atencao('Atenção!', 'Houve algum erro ao adicionar o cliente. <br />' +
								'Cliente não adicionado, tente outra vez. <br />' +
								'Erro: 001');
							// OCULTA O MODAL
							$('#myModal').modal('hide');
						}

					}, // END SUCESSO
					// RETORNO DO ERRO
					error      : function () {
						// MENSAGEM DE ERRO
						erro('Erro!', 'Desculpe, não foi possivel adicionar o cliente. <br />' +
							'Entre em contato com o Administrador. <br />' +
							'Erro: 002');
						// OCULTA O MODAL
						$('#myModal').modal('hide');
						//LIMPA OS CAMPOS
						$('#myForm')[0].reset();
					} // END ERRO
				});	// END AJAX
				// RETORNO DO SUBMIT
				return false;
			}
			return false;
		}); // END DO SUBMIT


	} // END FUNCTION ADINIONAR


	/** FUNÇÃO PARA EDITAR */
	function editMember(id = null) {
		// VERIFICA SE O ID EXISTE
		if (id) {

			// SIMULA O EVENTO QUE LIMPA O CAMPO DE ESCOLHER IMAGEM
			$('.remove').click();

			// LIMPA O FORM
			// SUBSTITUI O TÍTULO DO MODAL
			$('#myModal').find('.modal-title').text('Editar Cliente');
			// ADICIONA O CAMINHO DA AÇÃO DO FORM
			//$('#myForm').attr('action', '<?php //echo base_url() ?>cliente_c/edit/' + id);
			// LIMPA O FORM
			//$("#myForm")[0].reset();

			$('#img_atual*').fadeIn('fast');

			$('.ios-switch').removeClass('off').addClass('on').next('input:checkbox').attr('checked', true).val('1');

			$('.stt').html('Ativo');

			// CHAMA O AJAX
			$.ajax({
				url     : '<?php echo base_url() ?>cliente_c/selectById/' + id, // URL DO CAMINHO/CONTROLE/MÉTODO
				type    : 'post', // TIPO POST
				dataType: 'json',
				// RETORNO DO SUCESSO
				success : function (response) {
					// COLOCANDO VALRES NOS CAMPOS
					$("input[name=nome_cliente]").val(response.nome_cliente);
					$("input[name=cpf_cliente]").val(response.cpf_cliente);
					$("input[name=email_cliente]").val(response.email_cliente);
					$('.aqui').attr("src", "<?php echo base_url('assets/uploads/clientes/'); ?>" + response.imagem_cliente);
					$('input[name=id_cliente]').val(response.id_cliente);
					$('#summernote').summernote('code', response.obs_cliente);
					$("input[name=data_nascimento_cliente]").val(response.data_nascimento_cliente);
					$("select[name=sexo_cliente]").val(response.sexo_cliente);
					$("input[name=logradouro_cliente]").val(response.logradouro_cliente);
					$("input[name=numero_cliente]").val(response.numero_cliente);
					$("input[name=bairro_cliente]").val(response.bairro_cliente);
					$("input[name=cidade_cliente]").val(response.cidade_cliente);
					$("input[name=estado_cliente]").val(response.estado_cliente);
					$("input[name=pais_cliente]").val(response.pais_cliente);
					$("input[name=cep_cliente]").val(response.cep_cliente);
					$("input[name=celular_cliente]").val(response.celular_cliente);
					$("input[name=telefone_cliente]").val(response.telefone_cliente);


					if (response.status == '1') {

						$('.ios-switch').removeClass('off').addClass('on').next('input:checkbox').attr('checked', true).val('1');
						$('.stt').html('Ativo');

					} else {
						$('.ios-switch').removeClass('on').addClass('off').next('input:checkbox').removeAttr('checked').val('0');
						$('.stt').html('inativo');
					}

					// SUBMIT DO FORMULÁRIO PARA ATUALIZAR
					$("#myForm").unbind('submit').bind('submit', function () {

						var obs = $('input[name=obs_cliente]');

						//PEGA OS VALORES EXATOS DO EDITOR
						var markupStr = $('#summernote').summernote('code');
						//SETA NO VALOR DA VARIÁVEL TITULO
						obs.val(markupStr);

						// PEGA O FORMULÁRIO
						var form = $(this);

						// PEGA O FORMULÁRIO
						var formData = new FormData(this);

						// CHAMA O AJAX
						$.ajax({
							url        : '<?php echo base_url() ?>cliente_c/edit/' + id, // URL DO CAMINHO/CONTROLE/MÉTODO
							type       : 'post', // TIPO POST
							//data    : form.serialize(), // SERIALIZA O FORM
							data       : formData,
							processData: false,
							contentType: false,
							dataType   : 'json',
							// RETORNO DO SUCESSO
							success    : function (response) {
								// SE SUCESSO FOR VERDADEIRO
								if (response.success === true) {
									// MENSAGEM DE SUCESSO
									sucesso('Sucesso!', 'Cliente atualizado.');
									// OCUTA O MODAL EDIT
									$("#myModal").modal('hide');
									// RECARREGA A TABELA
									manageMemberTable.ajax.reload(null, false);
								} else {
									// MENSAGEM DE ATENÇÃO
									atencao('Atenção!', 'Houve algum erro ao atualizar o cliente. <br />' +
										'Cliente não atualizado, tente outra vez. <br />' +
										'Erro: 001');
									// OCULTA O MODAL
									$('#myModal').modal('hide');
								}
							}, // END SUCESSO ATUALIZAR
							// RETORNO DO ERRO
							error      : function () {
								// MENSAGEM DE ERRO
								erro('Erro!', 'Desculpe, não foi possivel atualizar o cliente. <br />' +
									'Entre em contato com o Administrador. <br />' +
									'Erro: 002');
								// OCULTA O MODAL
								$('#myModal').modal('hide');
							} // END ERRO ATUALIZAR

						});	// END AJAX ATUALIZAR
						// RETORNO DO SUNMIT
						return false;
					}); // END DO SUBMIT
				} // END SUCESSO EDITAR

			}); // END AJAX EDITAR

		} // END DO IF
		else {
			// SETTIMEOUT SÓ PARA DAR TEMPO DE IDENTIFICAR O MODAL
			setTimeout(() => {
				// OCULTA O MODAL
				$('#myModal').modal('hide');
			}, 500);
			// MENSAGEM DE ATENÇÃO
			atencao('Atenção!', 'Houve algum erro ao atualizar o cliente. <br />' +
				'Não foi passado o (id) do cliente. <br />' +
				'Erro: 003');
		} // END DO ELSE
	} // END DA FUNCTION EDITAR


	/** FUNÇÃO PARA REMOVER */
	function removeMember(id = null) {
		// VERIFICA SE O ID EXISTE
		if (id) {
			//SETTIMEOUT SÓ PARA DAR TEMPO DE IDENTIFICAR O MODAL
			setTimeout(() => {
				// ADICIONA TEXTO NO MODAL EXCLUIR
				$('#removeMemberModal').find('.modal-body').text("Você deseja excluir este cliente?");
			}, 500);
			// EVENTO DO BOTÃO PARA EXCLUIR
			$("#removeMemberBtn").unbind('click').bind('click', function () {
				// CHAMA O AJAX
				$.ajax({
					url     : '<?php echo base_url() ?>cliente_c/remove/' + id, // URL DO CAMINHO/CONTROLE/MÉTODO
					type    : 'post', // TIPO
					dataType: 'json',
					// RETORNO DO SUCESSO
					success : function (response) {
						if (response.success === true) {
							// MENSAGEM DE SUCESSO
							sucesso('Sucesso!', 'Cliente excluído.');
							// OCUTA O MODAL EXCLUIR
							$("#removeMemberModal").modal('hide');
							// RECARREGA A TABELA
							manageMemberTable.ajax.reload(null, false);
						} else {
							// MENSAGEM DE ATENÇÃO
							atencao('Atenção!', 'Houve algum erro ao excluir o cliente. <br />' +
								'Cliente não excluído, tente outra vez. <br />' +
								'Erro: 001');
							// OCULTA O MODAL
							$('#removeMemberModal').modal('hide');
						} // END ELSE
					}, // END SUCESSO
					// RETORNO DO ERRO
					error   : function () {
						// MENSAGEM DE ERRO
						erro('Erro!', 'Desculpe, não foi possivel excluir o cliente. <br />' +
							'Entre em contato com o Administrador. <br />' +
							'Erro: 002');
						// OCULTA O MODAL
						$('#removeMemberModal').modal('hide');
					} // END ERRO ATUALIZAR
				}); // END AJAX
			}); // END SUBMIT
		} // END IF
		else {
			// SETTIMEOUT SÓ PARA DAR TEMPO DE IDENTIFICAR O MODAL
			setTimeout(() => {
				// OCULTA O MODAL
				$('#removeMemberModal').modal('hide');
			}, 500);
			// MENSAGEM DE ATENÇÃO
			atencao('Atenção!', 'Houve algum erro ao excluir o cliente. <br />' +
				'Não foi passado o (id) do cliente. <br />' +
				'Erro: 003');
		} // END DO ELSE
	} // END DA FUNCTION EDITAR


	/** FUNÇÃO PARA EXCLUIR MULTIPLOS */
	function removeMultiple() {
		// PEGA A QUANTIDADE DE LINHAS SELECIONADAS
		var qtd_selected     = manageMemberTable.rows('.selected').data().length;
		// PEGA A QUANTIDADE DE LINHAS EXIBIDA POR PÁGINA
		var resultadosPorPag = $('#myForm2')[0][0].value;
		// PEGA A QTD DE OBJETOS DO FORM
		var qtdObjetos       = $('#myForm2')[0].length;
		// INICIA UMA VAR PARA PEGAR A QTD DE CHECKBOXES MARCADOS
		var qtdChecked       = 0;

		// FAZ UM LOOP PARA PERCORRER OS INPUTS DO FORM E VERIFICAR SE ESTÁ CHECKED
		for (var i = -1; i < qtdObjetos - 4; i++) { // ESSE 4 É DE ELEMENTOS DO FORM Q Ñ TEM (.CHECKED)
			if ($('#myForm2')[0][4 + i].checked) {
				qtdChecked += 1;
			} // END IF
		} // END FOR

		// VAR DE APOIO PARA LINHAS
		var clientes = "cliente";
		// VERIFICA SE TEM MAIS DE UMA LINHA SELECIONADA PARA ACRESCENTAR O 'S'
		if (qtdChecked > 1) clientes += "s";

		if (qtdChecked == 0) {
			// MENSAGEM DE ATENÇÃO
			atencao('Atenção!', 'Favor escolha pelo menos um cliente para exluir.');
			//SETTIMEOUT SÓ PARA DAR TEMPO DE IDENTIFICAR O MODAL
			setTimeout(() => {
				// OCULTA O MODAL
				$('#removeMemberModal').modal('hide');
			}, 500);

		} else {
			//SETTIMEOUT SÓ PARA DAR TEMPO DE IDENTIFICAR O MODAL
			setTimeout(() => {
				// ADICIONA TEXTO NO MODAL EXCLUIR
				$('#removeMemberModal').find('.modal-body').text("Você deseja excluir " + qtdChecked + " " + clientes + "?");
			}, 500);

			// SERIALIZA O FORM PARA PASSAR PARA O CONTROLE
			var data = $('#myForm2').serialize();

			// EVENTO DO BOTÃO PARA EXCLUIR
			$("#removeMemberBtn").unbind('click').bind('click', function () {
				// CHAMA O AJAX
				$.ajax({
					url     : '<?php echo base_url() ?>cliente_c/removeMultiple', // URL DO CAMINHO/CONTROLE/MÉTODO
					type    : 'ajax',  // AJAX
					method  : 'post',// POST
					data    : data,
					async   : false,
					dataType: 'json',
					// RETORNO DO SUCESSO
					success : function (response) {
						if (response.success === true) {
							// MENSAGEM DE SUCESSO
							sucesso('Sucesso!', 'Cliente(s) excluído(s).');
							// OCUTA O MODAL EXCLUIR
							$("#removeMemberModal").modal('hide');
							// RECARREGA A TABELA
							manageMemberTable.ajax.reload(null, false);
						} else {
							// MENSAGEM DE ATENÇÃO
							atencao('Atenção!', 'Houve algum erro ao excluir o(s) cliente(s). <br />' +
								'Clientes) não excluído(s), tente outra vez. <br />' +
								'Erro: 001');
							// OCULTA O MODAL
							$('#removeMemberModal').modal('hide');
						} // END ELSE
					}, // END SUCESSO
					// RETORNO DO ERRO
					error   : function () {
						// MENSAGEM DE ERRO
						erro('Erro!', 'Desculpe, não foi possivel excluir o(s) cliente(s). <br />' +
							'Entre em contato com o Administrador. <br />' +
							'Erro: 002');
						// OCULTA O MODAL
						$('#removeMemberModal').modal('hide');
					} // END ERRO ATUALIZAR
				}); // END AJAX
			}); // END SUBMIT
		} // END ELSE
	} // END FUNCTION EXLUIR MULTIPLO


	function download(img) {
		alert('Oi');
	}

</script>



