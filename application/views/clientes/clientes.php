<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Clientes - CadClient</title>

		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<!-- FavIcon -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/images/favicon.png">

		<!--INCLUINDO CSS-->
		<?php include_once VIEWPATH . 'includes/css.php'; ?>

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="<?php echo base_url('assets/admin/'); ?>assets/js/ace-extra.min.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="<?php echo base_url('assets/admin/'); ?>assets/js/html5shiv.min.js"></script>
		<script src="<?php echo base_url('assets/admin/'); ?>assets/js/respond.min.js"></script>
		<![endif]-->

	</head>

	<body class="no-skin">


		<!--preloader-->
		<div id="preloader">
			<div id="status"></div>
		</div>


		<!--INCLUINDO HEADER-->
		<?php include_once VIEWPATH . 'includes/header.php'; ?>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try {
					ace.settings.loadState('main-container');
				} catch (e) {
				}
			</script>

			<div id="sidebar" class="sidebar responsive ace-save-state">
				<script type="text/javascript">
					try {
						ace.settings.loadState('sidebar');
					} catch (e) {
					}
				</script>

				<!--INCLUINDO MENU-->
				<?php include_once VIEWPATH . 'includes/menu.php'; ?>

				<!-- =============================================== -->


				<!--INCLUINDO O MODAL DELETE-->
				<?php include_once VIEWPATH . 'includes/modal_delete.php'; ?>

			</div>

			<div class="main-content">
				<div class="main-content-inner">

					<div class="page-content">

						<div class="row">
							<div class="col-xs-12">
								<h3 class="header smaller lighter blue">Lista dos Clientes</h3>

								<div class="clearfix">
									<div class="pull-right tableTools-container"></div>
								</div>

								<!--MENSAGEM DE SUCESSO E DE ERRO-->
								<div class="alert alert-success" style="display: none;"></div>
								<div class="alert alert-danger" style="display: none;"></div>
								<div class="alert alert-warning" style="display: none;"></div>

								<div style="background-color: #438eb9">

									<a onClick="addMemberModel()" data-toggle="modal" data-target="#myModal" id="btnAdd"
									   class="btn btn-add btn-success">
										<i class="fa fa-plus"></i> Add Novo Cliente
									</a>
								</div>

								<!-- div.table-responsive -->

								<!-- div.dataTables_borderWrap -->

								<div class="messages"></div>

								<div>
									<form id="myForm2" action="" method="POST">

										<table id="dynamic-table" style="width:100%"
											   class="table table-striped table-bordered table-hover display responsive nowrap">
											<thead>
												<tr>
													<th style="width: 5%" class="center">
														<label class="pos-rel">
															<input id="checkboxPai" type="checkbox" class="ace" />
															<span class="lbl"></span>
														</label>
													</th>
													<th style="">Id</th>
													<th style="max-width: 10%">Imagem</th>
													<th style="">Nome</th>
													<th style="">CPF</th>
													<th style="">Celular</th>
													<th style="">E-mail</th>
													<th style="">Atualizado</th>
													<th style="">Status</th>
													<th style="">Ações</th>
												</tr>
											</thead>

										</table>

										<div style="background-color: #438eb9">
											<button id="btn_excluir_multiplos" onclick="removeMultiple()"
													data-toggle="modal" data-target="#removeMemberModal"
													class="btn btn-danger" type="button">
												<i class="fa fa-trash"></i>
												Excluir Marcados
											</button>
										</div>

									</form>

									<!--NOTIFIICAÇÕES-->
									<div class="col-sm-12">
                                        <span class="col-sm-4">
                                            <!--ESCURAS-->
                                            <input hidden="" id="gritter-light" checked="" type="checkbox"
												   class="ace ace-switch ace-switch-5" />
											<!--CLARAS-->
                                            <input hidden="" id="gritter-light" checked="" type="checkbox"
												   class="ace ace-switch ace-switch-5" />
                                            <span style="display: none" class="lbl middle"></span>
                                        </span>
									</div>

								</div>
							</div>
						</div>
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

				  <!--INCLUINDO FOOTER-->
			<?php include_once VIEWPATH . 'includes/footer.php'; ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->


		<!-- removeMember -->
		<div class="modal fade" tabindex="-1" role="dialog" id="removeMemberModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Confirmar Exclusão</h4>
					</div>
					<div class="modal-body">
						<p></p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<button type="button" id="removeMemberBtn" class="btn btn-primary">Excluir</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		<!-- removeMember -->

		<!--INCLUINDO O MODAL ADD-->
		<?php include_once VIEWPATH . 'clientes/modal_add.php'; ?>

		<!--INCLUINDO JS-->
		<?php include_once VIEWPATH . 'includes/js.php'; ?>

		<!--INCLUINDO AS FUNÇÕES DO AJAX(INSERIR, EDITAR, DELETAR E LISTAR)-->
		<?php include_once VIEWPATH . 'clientes/functions_js.php'; ?>

	</body>

</html>
