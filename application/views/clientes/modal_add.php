
<div id="myModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <!--<div class="btn-group" id="buttonlist"> 
                                <a class="btn btn-add " href="<?php //echo base_url('admin/bancos')          ?>"> 
                                    <i class="fa fa-list"></i>  Listar Banners </a>  
                            </div>-->
                    </div>
                    <div class="panel-body">
                        <form  id="myForm" action="" method="POST"  enctype="multipart/form-data" data-parsley-validate class="col-sm-12">

                            <input type="hidden" name="id_cliente" value="0">

                            <div class="form-group">
                                <label class="label-control">Nome</label>
                                <input name="nome_cliente" type="text" class="form-control" placeholder="Nome do cliente...">
                            </div>
                            
                            <br style="clear: both" />

							<div class="form-group">
								<label class="label-control">CPF</label>
								<input name="cpf_cliente" type="text" class="form-control" placeholder="CPF do cliente...">
							</div>

							<br style="clear: both" />

							<div class="form-group">
								<label class="label-control">E-mail</label>
								<input name="email_cliente" type="text" class="form-control" placeholder="E-mail do cliente...">
							</div>

							<br style="clear: both" />

							<div class="form-group">
								<label class="label-control">Data de Nascimento</label>
								<input name="data_nascimento_cliente" type="date" class="form-control" placeholder="Data de nascimento do cliente...">
							</div>

							<br style="clear: both" />

							<div class="form-group">
								<label class="label-control">Sexo</label>
								<select name="sexo_cliente" type="text" class="form-control">
									<option>Escolha seu sexo</option>
									<option value="Feminino">Feminino</option>
									<option value="Masculino">Masculino</option>
								</select>
							</div>

							<br style="clear: both" />

							<div class="form-group">
								<label class="label-control">Logradouro</label>
								<input name="logradouro_cliente" type="text" class="form-control" placeholder="Logradouro do cliente...">
							</div>

							<br style="clear: both" />

							<div class="form-group">
								<label class="label-control">Número</label>
								<input name="numero_cliente" type="text" class="form-control" placeholder="Número da casa do cliente...">
							</div>

							<br style="clear: both" />

							<div class="form-group">
								<label class="label-control">Bairro</label>
								<input name="bairro_cliente" type="text" class="form-control" placeholder="Bairro do cliente...">
							</div>

							<br style="clear: both" />

							<div class="form-group">
								<label class="label-control">Cidade</label>
								<input name="cidade_cliente" type="text" class="form-control" placeholder="Cidade do cliente...">
							</div>

							<br style="clear: both" />

							<div class="form-group">
								<label class="label-control">Estado - UF</label>
								<input name="estado_cliente" type="text" class="form-control" placeholder="UF do cliente...">
							</div>

							<br style="clear: both" />

							<div class="form-group">
								<label class="label-control">País</label>
								<input name="pais_cliente" type="text" class="form-control" placeholder="País do cliente...">
							</div>

							<br style="clear: both" />

							<div class="form-group">
								<label class="label-control">CEP</label>
								<input name="cep_cliente" type="text" class="form-control" placeholder="CEP do cliente...">
							</div>

							<br style="clear: both" />

							<div class="form-group">
								<label class="label-control">Celular</label>
								<input name="celular_cliente" type="text" class="form-control" placeholder="Celular do cliente...">
							</div>

							<br style="clear: both" />

							<div class="form-group">
								<label class="label-control">Telefone</label>
								<input name="telefone_cliente" type="text" class="form-control" placeholder="Telefone do cliente...">
							</div>

							<br style="clear: both" />

							<div class="form-group">
								<input name="obs_cliente" type="hidden" class="form-control">
							</div>

							<!--EDITOR SUMMERNOTE-->
							<label class="label-control">Observações</label>
							<div id="summernote"></div>

							<br style="clear: both" />


							<div id="img_atual" class="form-group">

								<label for="imagem" class="label-control " style="padding-left: 0">Imagem Atual</label>

								<img style="border: 1px #ccc solid; padding: 10px" class="col-md-12 aqui"  src="" />

								<br style="clear: both" />

							</div>

							<div class="slide-primary">
								<label>Status</label>
								<input  type="checkbox" name="status" class="ios"  checked="checked" value="1"/>
								<i class="stt"></i>
							</div>

							<br style="clear: both" />

							<div class="form-group">

								<label for="id-input-file-2">Imagem do cliente</label>
								<input type="file" name="imagem_cliente" id="id-input-file-2" value=""/>

							</div>

                            <br style="clear: both" />

                            <div class="reset-button">
                                <input id="cornerExpand" type="reset" class="btn btn-warning" value="Limpar"/>
                                <button id="btnSave" type="submit" class="btn btn-success">Salvar</button>
                                <!--<input type="submit" class="btn btn-success" value="Salvar"/>-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

