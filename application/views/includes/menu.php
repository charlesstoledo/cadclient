<!--MENU-->

<?php

	/** PARTES DA URL APÓS O NOME DO DOMÍNIO .COM.BR **/
	$parte_1 = $this->uri->segment(1);
	$parte_2 = $this->uri->segment(2);
	$parte_3 = $this->uri->segment(3);

	$nivel  = $this->session->userdata('nivel');
?>

<ul class="nav nav-list">
	<!--VERIFICA QUAL PÁGINA ESTÁ PARA ATIVAR O ITEM DO MENU-->
	<li <?php
		if ($parte_2 == 'home') {
			echo 'class=active';
		}
	?>>
		<a href="<?php echo base_url('home'); ?>">
			<i class="menu-icon fa fa-home"></i>
			<span class="menu-text"> Início </span>
		</a>
	</li>

	<!--CLIENTES-->
	<li <?php
		if ($parte_2 == 'clientes') {
			echo 'class=active';
		}
	?>>
		<a href="<?php echo base_url('clientes'); ?>">
			<i class="menu-icon fa fa-user"></i>
			<span class="menu-text"> Clientes </span>
		</a>
	</li>

	<?php
		//SÓ APARECE NA VIEW SE O USUÁRIO FOR MASTER
		if ($nivel == !null && $nivel == 2) {
			?>

			<li <?php
				if ($parte_2 == 'usuarios') {
					echo 'class=active';
				}
			?>>
				<a href="<?php echo base_url('usuarios'); ?>">
					<i class="menu-icon fa fa-users"></i>
					<span class="menu-text"> Usuários </span>
				</a>
			</li>

			<li <?php
				if ($parte_2 == 'logs') {
					echo 'class=active';
				}
			?>>
				<a href="<?php echo base_url('logs'); ?>">
					<i class="menu-icon fa fa-user-secret"></i>
					<span class="menu-text"> Logs </span>
				</a>
			</li>

			<?php
		}
	?>


</ul><!-- /.nav-list -->

<!--MINIMIZAR O MENU-->
<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
	<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state"
	   data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
</div>
