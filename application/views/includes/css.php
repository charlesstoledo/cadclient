
<!--CSS-->
<!-- bootstrap & fontawesome -->
<link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>assets/css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>assets/font-awesome/4.5.0/css/font-awesome.min.css" />
<!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/datatables/datatables.min.css" /> -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datatables/css/select.dataTables.min.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datatables/css/responsive.dataTables.min.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datatables/css/buttons.dataTables.min.css" />





<!-- page specific plugin styles -->
<!--NOTIFICAÇÕES-->
<link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>assets/css/jquery-ui.custom.min.css" />
<link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>assets/css/jquery.gritter.min.css" />


<!-- text fonts -->
<link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>assets/css/fonts.googleapis.com.css" />

<!-- ace styles -->
<link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

<!--[if lte IE 9]>
        <link rel="stylesheet" href="<?php //echo base_url('assets/admin/'); ?>assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
<![endif]-->
<link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>assets/css/ace-skins.min.css" />
<link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>assets/css/ace-rtl.min.css" />

<!--MEU ESTILO-->
<link rel="stylesheet" href="<?php echo base_url('assets/admin/'); ?>assets/css/style.css" />

<!--[if lte IE 9]>
  <link rel="stylesheet" href="<?php //echo base_url('assets/admin/'); ?>assets/css/ace-ie.min.css" />
<![endif]-->

<!--SWITCH DE STATUS DOS FORM-->
<link href="<?php echo base_url('assets/admin/'); ?>assets/js/ios-switch/ios7-switch.css" rel="stylesheet" type="text/css" media="screen">

<!-- include summernote css/js-->
<link href="<?php echo base_url('assets/'); ?>summernote-0.8.8-dist/dist/summernote.css" rel="stylesheet">