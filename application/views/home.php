<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>Home - CadClient</title>

        <meta name="description" content="Sistema CRUD" />
        <meta name="author" content="Charles Jr" />
        <meta name="keywords" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        
        <!-- FavIcon -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/images/favicon.png">

        <!--INCLUINDO CSS-->
        <?php include_once VIEWPATH . '/includes/css.php'; ?>
        
        <!-- ace settings handler -->
        <script src="<?php echo base_url('assets/admin/'); ?>assets/js/ace-extra.min.js"></script>

        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
        <!--[if lte IE 8]>
        <script src="<?php echo base_url('assets/admin/'); ?>assets/js/html5shiv.min.js"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>assets/js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body class="no-skin">

        <!--preloader-->
        <div id="preloader">
            <div id="status"></div>
        </div>

        <!--INCLUINDO HEADER-->
        <?php include_once VIEWPATH . '/includes/header.php'; ?>

        <div class="main-container ace-save-state" id="main-container">
            <div id="sidebar" class="sidebar responsive ace-save-state">

                <!--INCLUINDO MENU-->
                <?php include_once VIEWPATH . '/includes/menu.php'; ?>

            </div>

            <div class="main-content">
                <div class="main-content-inner">
                    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <b style="margin-left: 15px; font-size: 12pt">Olá, seja bem-vindo!</b>
                    </div>

                    <div class="page-content">

                        <div class="row">
                            <div class="col-xs-12">
                                <!-- PAGE CONTENT BEGINS -->
                                <div class="row">

                                    <div class="space-6"></div>

                                    <div class=" col-sm-12 col-md-12 col-lg-12 col-xs-12 infobox-container">

                                        <?php
                                        //SÓ APARECE NA VIEW SE O USUÁRIO FOR MASTER
                                        if ($nivel == !null && $nivel == 2) {
                                            echo "<h2>Monitoramento</h2>";
                                        }else{
                                            echo "<h2>Início</h2>";
                                            echo "<h2>Use o Menu a esquerda para interagir.</h2>";
                                        }
                                        ?>
                                        
                                        <?php
                                        //VERIFICA O NÍVEL DO USUÁRIO PARA HABILITAR MENU DE USUÁRIO E LOGS
                                        if ($nivel == !null && $nivel == 2) {
                                            ?>
                                            <a href="<?php echo base_url('usuarios'); ?>">
                                                <div id="jq_btn_cad" class="infobox infobox-blue2">
                                                    <div class="infobox-icon">
                                                        <i style="background: #f87c09" class="ace-icon fa fa-users"></i>
                                                    </div>

                                                    <div class="infobox-data">
                                                        <span  style="color: #f87c09" class="infobox-data-number">
                                                            <?php
                                                            //CONTAGEM DOS DADOS DO BANCO
                                                            if (isset($usuarios)) {
                                                                echo count($usuarios);
                                                            }
                                                            ?>
                                                        </span>
                                                        <div class="infobox-content">Usuários</div>
                                                    </div>
                                                </div>
                                            </a>

                                            <a href="<?php echo base_url('logs'); ?>">
                                                <div id="jq_btn_cad" class="infobox infobox-grey">
                                                    <div class="infobox-icon">
                                                        <i class="ace-icon fa fa-user-secret"></i>
                                                    </div>

                                                    <div class="infobox-data">
                                                        <span class="infobox-data-number">
                                                            <?php
                                                            //CONTAGEM DOS DADOS DO BANCO
                                                            if (isset($logs)) {
                                                                echo count($logs);
                                                            }
                                                            ?>
                                                        </span>
                                                        <div class="infobox-content">Logs</div>
                                                    </div>
                                                </div>
                                            </a>
                                            <?php
                                        }//FIM DA VERIFICA DO NÍVEL DE USUÁRIO
                                        ?>

                                    </div>
                                </div><!-- /.row -->

                                <!-- PAGE CONTENT ENDS -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.page-content -->
                </div>
            </div><!-- /.main-content -->

            <!--INCLUINDO FOOTER-->
            <?php include_once VIEWPATH . '/includes/footer.php'; ?>

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

        <!--INCLUINDO JS-->
        <?php include_once VIEWPATH . '/includes/js.php'; ?>

    </body>
</html>
