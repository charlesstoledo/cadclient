<?php
    //VERIFICAÇÃO MANUAL SE O USUÁRIO ATUAL É MASTER | APENAS MASTER ACESSA ESTA PAG
    $nivel = $this->session->userdata('nivel');
    
    if($nivel !== "2"){
        redirect("home");
    }

?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>Usuários - CadClient</title>

        <meta name="description" content="overview &amp; stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <!-- FavIcon -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/images/favicon.png">

        <!--INCLUINDO CSS-->
        <?php include_once VIEWPATH . 'includes/css.php'; ?>

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->
        <script src="<?php echo base_url('assets/admin/'); ?>assets/js/ace-extra.min.js"></script>

        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

        <!--[if lte IE 8]>
        <script src="<?php echo base_url('assets/admin/'); ?>assets/js/html5shiv.min.js"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>assets/js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body class="no-skin">


        <!--preloader-->
        <div id="preloader">
            <div id="status"></div>
        </div>

        <!--INCLUINDO HEADER-->
        <?php include_once VIEWPATH . 'includes/header.php'; ?>

        <div class="main-container ace-save-state" id="main-container">
            <script type="text/javascript">
                try {
                    ace.settings.loadState('main-container');
                } catch (e) {
                }
            </script>

            <div id="sidebar" class="sidebar responsive ace-save-state">
                <script type="text/javascript">
                    try {
                        ace.settings.loadState('sidebar');
                    } catch (e) {
                    }
                </script>

                <!--INCLUINDO MENU-->
                <?php include_once VIEWPATH . 'includes/menu.php'; ?>

                <!-- =============================================== -->

                <!--INCLUINDO O MODAL ADD E UPDATE-->
                <?php include_once VIEWPATH . 'usuarios/modal_add.php'; ?>

                <!--INCLUINDO O MODAL DELETE-->
                <?php include_once VIEWPATH . 'includes/modal_delete.php'; ?>

            </div>

            <div class="main-content">
                <div class="main-content-inner">

                    <div class="page-content">

                        <div class="row">
                            <div class="col-xs-12">
                                <h3 class="header smaller lighter blue">Lista dos Usuários</h3>

                                <div class="clearfix">
                                    <div class="pull-right tableTools-container"></div>
                                </div>

                                <!--MENSAGEM DE SUCESSO E DE ERRO-->
                                <div class="alert alert-success" style="display: none;"></div>
                                <div class="alert alert-danger"  style="display: none;"></div>
                                <div class="alert alert-warning" style="display: none;"></div>

                                <div style="background-color: #438eb9">

                                    <a id="btnAdd" class="btn btn-add btn-success" > 
                                        <i class="fa fa-plus"></i> Add Novo Usuário
                                    </a>
                                </div>

                                <!-- div.table-responsive -->

                                <!-- div.dataTables_borderWrap -->
                                <div>
                                    <form id="myForm2" action="" method="POST">

                                        <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="width: 4%" class="center">
                                                        <label class="pos-rel">
                                                            <input type="checkbox" class="ace" />
                                                            <span class="lbl"></span>
                                                        </label>
                                                    </th>
                                                    <th>Imagem</th>
                                                    <th>Nome</th>
                                                    <th style="width: 10%">Nível</th>
                                                    <th style="width: 15%" class="hidden-480">Senha</th>

                                                    <th style="width: 10%">
                                                        <i class="ace-icon fa fa-clock-o bigger-110 hidden-480"></i>
                                                        Atualizado
                                                    </th>
                                                    <th class="hidden-480">Status</th>

                                                    <th>Ações</th>
                                                </tr>
                                            </thead>

                                            <tbody id="showdata">

                                            </tbody>

                                        </table>

                                        <div style="background-color: #438eb9">
                                            <button class="btn btn-danger jq_btn_excluir_marcados" type="button" >
                                                <i class="fa fa-trash"></i>
                                                Excluir Marcados
                                            </button>

                                        </div>

                                    </form>

                                    <!--NOTIFIICAÇÕES-->
                                    <div class="col-sm-12">
                                        <span class="col-sm-4">
                                            <!--ESCURAS-->
                                            <input hidden="" id="gritter-light" checked="" type="checkbox" class="ace ace-switch ace-switch-5" />
                                            <!--CLARAS-->
                                            <input hidden="" id="gritter-light" checked="" type="checkbox" class="ace ace-switch ace-switch-5" />
                                            <span style="display: none" class="lbl middle"></span>
                                        </span> 
                                    </div> 

                                </div>
                            </div>
                        </div>
                    </div><!-- /.page-content -->
                </div>
            </div><!-- /.main-content -->

            <!--INCLUINDO FOOTER-->
            <?php include_once VIEWPATH . 'includes/footer.php'; ?>

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

        <!--INCLUINDO JS-->
        <?php include_once VIEWPATH . 'includes/js.php'; ?>

        <!--INCLUINDO AS FUNÇÕES DO AJAX(INSERIR, EDITAR, DELETAR E LISTAR)-->
        <?php include_once VIEWPATH . 'usuarios/functions_js.php'; ?>

    </body>

</html>
