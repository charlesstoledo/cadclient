<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Login - CadClient</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- FavIcon -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/images/favicon.png">

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="<?php echo base_url('assets/admin/assets/css/bootstrap.min.css'); ?>" />
		<link rel="stylesheet"
			  href="<?php echo base_url('assets/admin/assets/font-awesome/4.5.0/css/font-awesome.min.css'); ?>" />

		<!-- text fonts -->
		<link rel="stylesheet" href="<?php echo base_url('assets/admin/assets/css/fonts.googleapis.com.css'); ?>" />

		<!-- ace styles -->
		<link rel="stylesheet" href="<?php echo base_url('assets/admin/assets/css/ace.min.css'); ?>" />

		<!--[if lte IE 9]>
		<link rel="stylesheet" href="<?php echo base_url('assets/admin/assets/css/ace-part2.min.css'); ?>" />
		<![endif]-->
		<link rel="stylesheet" href="<?php echo base_url('assets/admin/assets/css/ace-rtl.min.css'); ?>" />

		<!--[if lte IE 9]>
		<link rel="stylesheet" href="<?php echo base_url('assets/admin/assets/css/ace-ie.min.css'); ?>" />
		<![endif]-->

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="<?php echo base_url('assets/admin/assets/js/html5shiv.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/admin/assets/js/respond.min.js'); ?>"></script>
		<![endif]-->
	</head>

	<body class="login-layout">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
									<i class="ace-icon fa fa-leaf green"></i>
									<span class="red">Painel</span>
									<span class="white" id="id-text2">Administrativo</span>
								</h1>
								<h4 class="blue" id="id-company-text">&copy; CadClient</h4>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												<i class="ace-icon fa fa-coffee green"></i>
												Preencha as suas credenciais
											</h4>

											<div class="space-6"></div>

											<form id="MyForm" action="" method="POST" enctype="multipart/form-data"
												  data-parsley-validate>
												<fieldset>
													<label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input name="usuario" type="text" class="form-control"
																   placeholder="Usuário" required="" />
                                                            <i class="ace-icon fa fa-user"></i>
                                                        </span>
													</label>

													<label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input name="senha" type="password" class="form-control"
																   placeholder="Senha" required="" />
                                                            <i class="ace-icon fa fa-lock"></i>
                                                        </span>
													</label>

													<div class="space"></div>


													<div id="div_recaptcha">
														<div id="recaptcha" class="g-recaptcha"
															 data-sitekey="6LdYiVQUAAAAAFtTaN07U2Y4NRU1Wslbxtb-tSB0"></div>
													</div>


													<br>

													<div class="clearfix">

														<button id="MyBtn" type="button"
																class="width-35 pull-right btn btn-sm btn-primary">
															<i class="ace-icon fa fa-key"></i>
															<span class="bigger-110">Entrar</span>
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>

											<!--MSG DE ERRO-->
											<section id="jq_msg"></section>

										</div><!-- /.widget-main -->

									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->
							</div><!-- /.position-relative -->
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->

			  <!-- basic scripts -->

			  <!--[if !IE]> -->
		<script src="<?php echo base_url('assets/admin/assets/js/jquery-2.1.4.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/admin/assets/js/ace-elements.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/admin/assets/js/ace.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/admin/assets/js/bootstrap.min.js'); ?>"></script>

			  <!-- page specific plugin scripts -->

			  <!--[if lte IE 8]>
		<script src="<?php echo base_url('assets/admin/assets/js/excanvas.min.js'); ?>"></script>
			  <![endif]-->

		<script src="<?php echo base_url('assets/admin/assets/js/spin.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/admin/assets/js/form.js'); ?>"></script>

			  <!-- <![endif]-->

			  <!--[if IE]>
		<script src="<?php echo base_url('assets/admin/assets/js/jquery-1.11.3.min.js'); ?>"></script>
			  <![endif]-->
		<script type="text/javascript">
			if ('ontouchstart' in document.documentElement)
				document.write("<script src='<?php echo base_url('assets/admin/assets/js/jquery.mobile.custom.min.js'); ?>'>" + "<" + "/script>");
		</script>

			  <!-- SCRIPT DO RECAPTCHA-->
		<script src='https://www.google.com/recaptcha/api.js'></script>

			  <!--INCLUINDO FUNC JS DAS MSGS-->
		<?php include_once VIEWPATH . 'includes/func_login_js.php'; ?>

	</body>
</html>
