<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class MY_Geral_Model extends CI_Model
	{

		//CONSTRUTOR
		function __construct()
		{
			parent::__construct();

		}


		/**
		 * @function select simples
		 * @param array $params (tabela,coluna_ordenada,tipo_ordenacao, limite, status, excluido)
		 * @return bool
		 */
		public function select($params = array(null))
		{
			//ORDENAR
			if (isset($params['coluna_ordenada']) && isset($params['tipo_ordenacao'])) {

				$this->db->order_by($params['coluna_ordenada'], $params['tipo_ordenacao']);
			}

			//LIMITE
			if (isset($params['limite'])) {

				$this->db->limit($params['limite']);
			}

			//CONDIÇÃO STATUS
			if (isset($params['status'])) {

				$this->db->where('status', $params['status']);
			}


			//CONDIÇÃO EXCLUIDO
			if (isset($params['excluido'])) {

				$this->db->where('excluido', $params['excluido']);
			}

			// EXECULTA A CONSULTA NO BANCO
			if (isset($params['tabela'])) {

				$query = $this->db->get($params['tabela']);

				//VERIFICA SE EXISTE DADOS PARA RETORNAR
				if ($query->num_rows() > 0) {
					return $query->result_array();
				} else {
					return false;
				}
			} else {
				return false;
			}

		}


		/**
		 * @function select pelo id
		 * @param array $params (tabela, coluna_id, valor_id, status, excluido)
		 * @return bool
		 */
		public function selectById($params = array(null))
		{
			//CONDIÇÃO ID
			if (isset($params['coluna_id']) && isset($params['valor_id'])) {

				$this->db->where($params['coluna_id'], $params['valor_id']);
			}

			//CONDIÇÃO STATUS
			if (isset($params['status'])) {

				$this->db->where('status', $params['status']);
			}

			//CONDIÇÃO EXCLUIDO
			if (isset($params['excluido'])) {

				$this->db->where('excluido', $params['excluido']);
			}

			//TABELA
			if (isset($params['tabela'])) {
				$query = $this->db->get($params['tabela']);

				//VERIFICA SE EXISTE DADOS PARA RETORNAR
				if ($query->num_rows() > 0) {
					return $query->row_array();
				} else {
					return false;
				}
			} else {
				return false;
			}
		}


		/**
		 * @function create
		 * @param array $params (tabela, dados(array))
		 * @return bool
		 */
		public function create($params = array(null))
		{
			//INSERIRDO NO BANCO
			if (isset($params['tabela']) && isset($params['dados'])) {

				$query = $this->db->insert($params['tabela'], $params['dados']);
			}

			// VERIFICA SE INSERIU
			if ($query === true) {
				return true;
			} else {
				return false;
			}
		}


		/**
		 * @function edit
		 * @param array $params (tabela, coluna_id, valor_id, dados(array))
		 * @return bool
		 */
		public function edit($params = array(null))
		{
			//CONDIÇÃO ID
			if (isset($params['coluna_id']) && isset($params['valor_id'])) {
				$this->db->where($params['coluna_id'], $params['valor_id']);
			}

			//CONDIÇÃO TABELA
			if (isset($params['tabela']) && isset($params['dados'])) {
				$query = $this->db->update($params['tabela'], $params['dados']);
			}

			// VERIFICA SE FOI ATUALIZADO
			if ($query === true) {
				return true;
			} else {
				return false;
			}

		}


		/**
		 * @function remove
		 * @param array $params (tabela, coluna_id, valor_id)
		 * @return bool
		 */
		public function remove($params = array(null))
		{
			//CONDIÇÃO ID
			if (isset($params['coluna_id']) && isset($params['valor_id'])) {
				//CONDIÇÃO PASSANDO O ID DO BANNER QUE VAI SER DELETADO
				$this->db->where($params['coluna_id'], $params['valor_id']);
			}

			//TABELA
			if (isset($params['tabela'])) {
				$query = $this->db->delete($params['tabela']);
			}

			// RETORNA O RESULTADO DA QUERY
			return ($query === true) ? true : false;
		}


		/**
		 * @function
		 * @param array $params (tabela, coluna_id, valores_ids(array))
		 * @return bool
		 */
		function removeMultiple($params = array(null))
		{
			//CONDIÇÃO ID
			if (isset($params['coluna_id']) && isset($params['valores_ids'])) {
				$this->db->where_in($params['coluna_id'], $params['valores_ids']);
			}

			//TABELA
			if (isset($params['tabela'])) {
				$this->db->delete($params['tabela']);
			}

			//VERIFICA SE DELETOU
			if ($this->db->affected_rows() > 0) {
				return true;
			} else {
				return false;
			}
		}


		/**
		 * @function upload de imagem
		 * @param array $params (nome_imagem, nome_pasta)
		 * @return string
		 */
		public function upload($params = array(null))
		{

			if (isset($params['nome_imagem']) && $params['nome_pasta']) {
				$imagem = $params['nome_imagem'];
				$pasta  = $params['nome_pasta'];

				//SE EXIXTIR INPUT FILE COM ESSE NAME
				if (isset($_FILES[$imagem])) {

					//DEFINE O PATH ONDE O ARQUIVO SERÁ GRAVADO
					$path = "./assets/uploads/$pasta/";

					//VERIFICA SE EXISTE
					//SE NÃO EXISTIR CRIAMOS COM PERMISSÃO DE LEITURA E ESCRITA
					if (!is_dir($path)) {
						mkdir($path, 0644, $recursive = true);
					}
					//PEGA A EXTENÇÃO DA IMAGEM
					$extension = explode('.', $_FILES[$imagem]['name']);

					//VERIFICA A EXTENÇÃO
					if ($extension[1] == "png" || $extension[1] == "jpg" || $extension[1] == "jpeg" || $extension[1] == "gif") {

						//CRIA UM NOVE NOME PARA A IMAGEM
						$new_name = uniqid(md5($_FILES[$imagem]['name'])) . '.' . $extension[1];
						//DESTINO DA IMAGEM
						$destination = $path . $new_name;
						//SETA O DESTINO DA IMAGEM
						move_uploaded_file($_FILES[$imagem]['tmp_name'], $destination);
						//RETORNA O NOVO NOME DA IMAGEM
						return $new_name;
					}
				}

			}

		}


		/**
		 * @function download
		 * @param array $params (caminho_arquivo)
		 * @return null
		 */
		public function download($params = array(null))
		{
			if (isset($params['caminho_arquivo'])) {
				$caminho = $params['caminho_arquivo'];

				force_download("$caminho", NULL);
			} else {
				return null;
			}
		}


	}

