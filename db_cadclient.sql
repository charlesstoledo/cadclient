-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 04-Maio-2018 às 01:46
-- Versão do servidor: 5.6.16
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_cadclient`
--
CREATE DATABASE IF NOT EXISTS `db_cadclient` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `db_cadclient`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `acesso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_admin_log`
--

DROP TABLE IF EXISTS `tbl_admin_log`;
CREATE TABLE `tbl_admin_log` (
  `id` int(10) NOT NULL,
  `nome_admin` varchar(255) DEFAULT NULL,
  `acesso_admin` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_admin` varchar(255) DEFAULT NULL,
  `status_admin` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_clientes`
--

DROP TABLE IF EXISTS `tbl_clientes`;
CREATE TABLE `tbl_clientes` (
  `id_cliente` int(10) NOT NULL,
  `imagem_cliente` text,
  `nome_cliente` varchar(255) DEFAULT NULL,
  `cpf_cliente` varchar(255) DEFAULT NULL,
  `email_cliente` varchar(255) DEFAULT NULL,
  `data_nascimento_cliente` varchar(255) DEFAULT '',
  `sexo_cliente` varchar(50) DEFAULT '',
  `logradouro_cliente` varchar(255) DEFAULT '',
  `numero_cliente` varchar(255) DEFAULT '',
  `bairro_cliente` varchar(255) DEFAULT '',
  `cidade_cliente` varchar(255) DEFAULT '',
  `pais_cliente` varchar(255) DEFAULT '',
  `cep_cliente` varchar(255) DEFAULT '',
  `celular_cliente` varchar(255) DEFAULT '',
  `telefone_cliente` varchar(255) DEFAULT '',
  `obs_cliente` text,
  `cadastro` datetime DEFAULT CURRENT_TIMESTAMP,
  `atualizado` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1',
  `excluido` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_nivel`
--

DROP TABLE IF EXISTS `tbl_nivel`;
CREATE TABLE `tbl_nivel` (
  `id_nivel` int(11) NOT NULL,
  `descricao_nivel` varchar(255) NOT NULL,
  `valor_nivel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tbl_nivel`
--

INSERT INTO `tbl_nivel` (`id_nivel`, `descricao_nivel`, `valor_nivel`) VALUES
(1, 'Básico', 1),
(2, 'Administrador', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_usuarios`
--

DROP TABLE IF EXISTS `tbl_usuarios`;
CREATE TABLE `tbl_usuarios` (
  `id_usuario` int(10) NOT NULL,
  `imagem_usuario` text NOT NULL,
  `nome_usuario` varchar(255) NOT NULL,
  `email_usuario` varchar(255) NOT NULL,
  `senha_usuario` varchar(255) NOT NULL,
  `nivel_usuario` int(11) NOT NULL,
  `cadastrado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `atualizado` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `excluido` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tbl_usuarios`
--

INSERT INTO `tbl_usuarios` (`id_usuario`, `imagem_usuario`, `nome_usuario`, `email_usuario`, `senha_usuario`, `nivel_usuario`, `cadastrado`, `atualizado`, `status`, `excluido`) VALUES
(2, 'b8e0a6df08efc65a6d5418eb97f60f535ae83aa78c524.png', 'Admin', '', 'admin', 2, '2018-05-01 10:00:07', '2018-05-03 19:55:54', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `tbl_admin_log`
--
ALTER TABLE `tbl_admin_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_clientes`
--
ALTER TABLE `tbl_clientes`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indexes for table `tbl_nivel`
--
ALTER TABLE `tbl_nivel`
  ADD PRIMARY KEY (`id_nivel`);

--
-- Indexes for table `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin_log`
--
ALTER TABLE `tbl_admin_log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_clientes`
--
ALTER TABLE `tbl_clientes`
  MODIFY `id_cliente` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_nivel`
--
ALTER TABLE `tbl_nivel`
  MODIFY `id_nivel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  MODIFY `id_usuario` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
